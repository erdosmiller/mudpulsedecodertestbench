#ifndef DECODERCONTAINER_H
#define DECODERCONTAINER_H

#include <QObject>
#include "decoder.h"
#include <qlist.h>
#include <datasource.h>
#include <filterblock.h>
#include <qhash.h>
#include <qtimer.h>
#include <qdatetime.h>

#define MULTIPLE_FILTER_lIST "mfilterlist"
#define MULTIPLE_INSTANCE_CONFIGPATH "multipleinstanceconfigpath"
#define DECODER_NAME "decodername"

class DecoderMetaData
{
public:
    QString m_decoderName;
    int m_score;
    QString m_filterName;

    void bumpScore(){m_score++;}
};

class DecodeItem
{
public:

    QString m_name;
    float m_value;
    float m_confidence;
    float m_outofbandsnr;
    float m_inbandsnr;
    DecodeStatus m_status;
    int m_decodeIndex;
    Decoder* m_pDecoderIstance;

    bool operator==(const DecodeItem &rhs) {return this->m_pDecoderIstance == rhs.m_pDecoderIstance;}
};

class DecodeItemGroup
{
public:
    void addItem(DecodeItem item) {m_decodedItems.append(item); m_lastUpdated = 0;}
    void bumpAge() {m_lastUpdated++;}
    unsigned int getAge() {return m_lastUpdated;}
    bool hasItem(DecodeItem itemToCheck) {return m_decodedItems.contains(itemToCheck);}
    int groupSize() {return m_decodedItems.size();}

    QList<DecodeItem> m_decodedItems;
    unsigned int m_lastUpdated;
};

class DecoderContainer : public Decoder
{
    Q_OBJECT
public:
    explicit DecoderContainer(QObject *parent = nullptr);
    virtual ~DecoderContainer();
    virtual bool initialize(QHash<QString,QString> properties);

    bool setDecoders(QList<Decoder*> decoderlist, QList<FilterBlock*> filterblocklist, DataSource* pdatasrc);
    virtual void timerEvent();
    static QStringList getFilters(QHash<QString,QString> properties);
    static QList<QHash<QString,QString>> getDecoderConfigs(QHash<QString,QString> properties);
    static QHash<QString,QString> modifyProperties(QHash<QString,QString> originalprops, QHash<QString,QString> mods);
    virtual int getSamplesPerPoint();

    static QString statusDesc(DecodeStatus status);

    virtual void postInitPropSet(QHash<QString,QString> properties){}
    virtual void reportStatusPostInit(){}
    virtual void postWitsOutMsg(QString msg){}

    void writeFilterStats(QString path,  QHash<unsigned int, QStringList> oldstats);
    QHash<unsigned int, QStringList> readFilterStats(QString path);

    static QHash<QString,QHash<QString,QString>> getMultipleConfigs(QString path);

    static void dumpProperties(QHash<QString,QString> props);
    static QString makeAbsolute(QString dirpath, QString filepath);

private:
    DecodeItem vote(DecodeItemGroup* itemGroup);
    void allDataIn(DecodeItemGroup* itemGroup);
    bool validateItem(DecodeItem item);

    void logEvent(QString decoder, QString name, float value, float inbandsnr, float outofbandsnr, int confidence, QString status);
    void logEvent(QString decoder, QString name, QString value, QString inbandsnr, QString outofbandsnr, QString confidence, QString status);

    void flushAnnotations(float lastkey);
signals:

public slots:
    void rawDataAvaliable(QList<float> newdata);
    virtual void dataAvaliable(QList<float> raw, QList<float> filtered, float snr){}

    void ContainerSyncDetected(bool issurvey, float confidence, float outofbandsnr, float inbandsnr);
    void ContainerSeqenceIDDecoded(int sequenceid, bool issurvey, float confidence, float outofbandsnr, float inbandsnr, DecodeStatus status);
    void ContainerSymbolDecoded(unsigned int value, bool issurvey, float confidence, float outofbandsnr, float inbandsnr, DecodeStatus status);
    void ContainerTelemValueDecoded(QString name, float value, bool issurvey, float confidence, float outofbandsnr, float inbandsnr, DecodeStatus status);
    void ContainerFilteredDataAvailable(QList<QPointF> raw, QList<QPointF> filtered, QList<QPointF> pressure, QHash<float,DecoderAnnotation*> annotations);

private:
    QList<Decoder*> m_decoderList;
    QList<FilterBlock*> m_filterList;
    DataSource* m_pDataSource;

    QList<QHash<Decoder*,DecodeItem>> m_decodedItems;
    int m_currentIndex;
    QHash<Decoder*,DecodeItem> m_currentDecodeItemMap;
    QHash<QString, DecodeItemGroup*> m_decodeItemGroupings;
    //QQueue
    QList<Decoder*> m_respondedList;
    QList<Decoder*> m_blackList;
    QTimer m_decoderTimeout;
    int m_logFileKey;
    int m_logFileTableKey;
    QHash<Decoder*,QHash<float,DecoderAnnotation*>> m_annotationsCache;
    Decoder* m_plastDecoderInUse;
    Decoder* m_primaryDecoder;

    QHash<Decoder*,DecoderMetaData*> m_metaDataMap;
    QHash<FilterBlock*,Decoder*> m_decoderMap;
    QDateTime m_decoderStartTime;
    QHash<unsigned int, QStringList> m_filterStats;
};

#endif // DECODERCONTAINER_H
