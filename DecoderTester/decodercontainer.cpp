#include "decodercontainer.h"
#include "decoderproperties.h"
#include <qloggingcategory.h>
#include <qdatetime.h>
#include <qdir.h>
#include "utils.h"
#include "decoders/decoderimpl.h"


QLoggingCategory DECODERCONTAINER("DecoderContainer.EM");

DecoderContainer::DecoderContainer(QObject *parent) : Decoder(nullptr, parent)
{
    m_currentIndex = 0;
    m_pDataSource = nullptr;
    m_decoderTimeout.setSingleShot(true);
    m_logFileKey = -1;
    m_logFileTableKey = -1;
    m_plastDecoderInUse = nullptr;
    m_primaryDecoder = nullptr;
}


DecoderContainer::~DecoderContainer()
{
    qWarning(DECODERCONTAINER) << "inside destructor for DecoderContainer";
    if(m_logFileKey != -1)
    {
        Utils::closeFile(m_logFileKey);
    }

    if(m_logFileTableKey != -1)
    {
        Utils::closeFile(m_logFileTableKey);
    }

    for(int i=0;i<m_decoderList.size();i++)
    {
        delete m_decoderList.at(i);
    }

    for(int i=0;i<m_filterList.size();i++)
    {
        delete m_filterList.at(i);
    }

    m_filterList.clear();
    m_decoderList.clear();
}


bool DecoderContainer::initialize(QHash<QString,QString> properties)
{
    qInfo(DECODERCONTAINER) << "INITIALIZING the decoder container...";
    QStringList filterlist = getFilters(properties);
    QList<QHash<QString,QString>> decoderconfigs = getDecoderConfigs(properties);
    bool status = m_filterList.size() == m_decoderList.size();

    m_logFileKey = -1;
    m_logFileTableKey = -1;

    if(properties.contains(EVENT_LOG_FILE_PATH))
    {
        QString path = properties.value(EVENT_LOG_FILE_PATH);
        qCWarning(DECODERCONTAINER) << "Event log path" << EVENT_LOG_FILE_PATH;

        if(!path.isEmpty())
        {
            m_logFileKey = Utils::createLogFile(path, true, true);

            QStringList parts = path.split(".");

            qCWarning(DECODERCONTAINER) << "Path is not empty";

            if(parts.size() == 2)
            {
                QString newpath = parts.first() + "_table.csv";

                m_logFileTableKey = Utils::createLogFile(newpath, true, true);

                qCWarning(DECODERCONTAINER) << "Log File Table Key" << m_logFileTableKey;
                qCWarning(DECODERCONTAINER) << "Log File Key" << m_logFileKey;

                if(m_logFileTableKey != -1)
                {
                    Utils::writeLogFile(m_logFileTableKey, "TimeStamp,Pump State,Time On,Time Off,Pressure,Event Name,Symbol Value,Param Name,Param Value,In Band SNR,Out Band SNR,Confidence,Status");
                }
            }
        }
    }

    properties.remove(EVENT_LOG_FILE_PATH);

    if(status)
    {
        if(m_decoderList.isEmpty())
        {
            for(int i=0;i<decoderconfigs.size();i++)
            {

            }
        }

        for(int i=0;i<m_decoderList.size();i++)
        {
            Decoder* pdecoder = m_decoderList.at(i);
            FilterBlock* pfilterblock = m_filterList.at(i);
            QString filterpath = decoderconfigs.at(i).value(LOPASS_FILTER_PATH);    // dicey - should add some protection here
            QStringList filterpathparts = filterpath.split("/");
            QHash<QString,QString> modifiedprops = modifyProperties(properties, decoderconfigs.at(i));

            if(modifiedprops.contains(DECODER_NAME))
            {
                m_metaDataMap.value(pdecoder)->m_decoderName = modifiedprops.value(DECODER_NAME);
            }

            //qWarning(DECODERCONTAINER) << "Begin Decoder " << m_metaDataMap.value(pdecoder)->m_decoderName;
            qCInfo(DECODERCONTAINER) << "Dumping modified properties for decoder " << i;
            dumpProperties(modifiedprops);
//            properties.insert(LOPASS_FILTER_PATH, filterpath);
            pdecoder->initialize(modifiedprops);
            pfilterblock->initialize(modifiedprops);
            //qWarning(DECODERCONTAINER) << "End Decoder " << m_metaDataMap.value(pdecoder)->m_decoderName;


            m_metaDataMap.value(pdecoder)->m_filterName = filterpathparts.last();
        }
    }

    connect(&m_decoderTimeout, SIGNAL(timeout()), this, SLOT(timerEvent()));
    m_decoderStartTime = QDateTime::currentDateTime();
    m_filterStats = readFilterStats("filterstats.txt");
    return status;
}


bool DecoderContainer::setDecoders(QList<Decoder*> decoderlist, QList<FilterBlock*> filterblocklist, DataSource* pdatasrc)
{
    bool status = decoderlist.size() == filterblocklist.size();

    if(status)
    {
        m_decoderList = decoderlist;
        m_filterList = filterblocklist;
        m_pDataSource = pdatasrc;

        for(int i=0;i<m_decoderList.size();i++)
        {
            Decoder* pdecoder = m_decoderList.at(i);
            FilterBlock* pfilterblock = m_filterList.at(i);
            DecoderMetaData* pmetadata = new DecoderMetaData();

            pmetadata->m_score = 0;
            pmetadata->m_decoderName = "decoder_" + QString::number(i);

//            QObject::connect(m_pDataSource, SIGNAL(dataAvaliable(QList<float>)), pfilterblock, SLOT(inputDataAvaliable(QList<float>)));
            QObject::connect(pfilterblock, SIGNAL(dataAvaliable(QList<float>,QList<float>,float)), ((DecoderImpl*)pdecoder)->getSymbolDecoder(), SLOT(dataAvaliable(QList<float>,QList<float>,float)));
            QObject::connect(pdecoder, SIGNAL(telemValueDecoded(QString,float,bool,float,float,float,DecodeStatus)), this, SLOT(ContainerTelemValueDecoded(QString,float,bool,float,float,float,DecodeStatus)));

            if(i == 0)
            {
                m_primaryDecoder = pdecoder;
            }

            m_decoderMap.insert(pfilterblock, pdecoder);
            m_metaDataMap.insert(pdecoder, pmetadata);
        }

        QObject::connect(m_pDataSource, SIGNAL(dataAvaliable(QList<float>)), this, SLOT(rawDataAvaliable(QList<float>)));

    }

    return status;
}



void DecoderContainer::rawDataAvaliable(QList<float> newdata)
{
    for(int i=0;i<m_filterList.size();i++)
    {
        FilterBlock* pfilterblk = m_filterList.at(i);
        DecoderMetaData* pmeta = m_metaDataMap.value(m_decoderMap.value(pfilterblk));

        //qWarning(DECODERCONTAINER) << "Begin Decoder " << pmeta->m_decoderName;
        pfilterblk->inputDataAvaliable(newdata);
        //qWarning(DECODERCONTAINER) << "End Decoder " << pmeta->m_decoderName;
    }

}

void DecoderContainer::ContainerSyncDetected(bool issurvey, float confidence, float outofbandsnr, float inbandsnr)
{

}

void DecoderContainer::ContainerSeqenceIDDecoded(int sequenceid, bool issurvey, float confidence, float outofbandsnr, float inbandsnr, DecodeStatus status)
{

}

void DecoderContainer::ContainerSymbolDecoded(unsigned int value, bool issurvey, float confidence, float outofbandsnr, float inbandsnr, DecodeStatus status)
{

}

void DecoderContainer::ContainerFilteredDataAvailable(QList<QPointF> raw, QList<QPointF> filtered, QList<QPointF> pressure, QHash<float,DecoderAnnotation*> annotations)
{
    QObject* pobj = sender();
    Decoder* pdecoder = (Decoder*)pobj;
    QHash<float,DecoderAnnotation*> annotationbucket;
    QList<float> keys = annotations.keys();
    QHash<float,DecoderAnnotation*> annotationstoforward;

    if(m_annotationsCache.contains(pdecoder))
    {
        annotationbucket = m_annotationsCache.value(pdecoder);
    }

    for(int i=0;i<keys.size();i++)
    {
        float k = keys.at(i);

        annotationbucket.insert(k, annotations.value(k));
    }

    m_annotationsCache.insert(pdecoder, annotationbucket);

    if(m_plastDecoderInUse != nullptr)
    {
        if(m_annotationsCache.contains(m_plastDecoderInUse))
        {
            QHash<float,DecoderAnnotation*> annotationstouse = m_annotationsCache.value(m_plastDecoderInUse);
            float lastkey = 0;
            QList<float> keylist = annotationstouse.keys();

            qSort(keylist);

            for(int i=0;i<keylist.size();i++)
            {
                float key = keylist.at(i);
                DecoderAnnotation* pannotation = annotationstouse.value(key);

                annotationstouse.remove(key);
                annotationstoforward.insert(key, pannotation);

                if(pannotation->m_type == DecoderAnnotationType::TelemetryValue || pannotation->m_type == DecoderAnnotationType::SyncMarker)
                {
                    lastkey = key;
                    m_annotationsCache.insert(m_plastDecoderInUse, annotationstouse);
                    break;
                }
            }

            flushAnnotations(lastkey);
        }

        m_plastDecoderInUse = nullptr;
    }

    if(pdecoder == m_primaryDecoder)
    {
        emit filteredDataAvailable(raw, filtered, pressure, annotationstoforward);
    }

}

void DecoderContainer::ContainerTelemValueDecoded(QString name, float value, bool issurvey, float confidence, float outofbandsnr, float inbandsnr, DecodeStatus status)
{
    QObject* pobj = sender();
    Decoder* pdecoder = (Decoder*)pobj;
    DecodeItem ditem;

    DecoderMetaData* pmetadata = m_metaDataMap.value(pdecoder);

//    if(m_respondedList.contains(pdecoder))
//    {
//        allDataIn(true);
//    }


    if(m_currentDecodeItemMap.isEmpty())
    {
        m_currentIndex++;
    }

//    qWarning(DECODERCONTAINER) << "ContainerTelemValueDecoded from decoder " << ((DecoderImpl*)pdecoder)->getLoggerDecorator() << " parameter name = " << name;

    ditem.m_name = name;
    ditem.m_value = value;
    ditem.m_confidence = confidence;
    ditem.m_status = status;
    ditem.m_inbandsnr = inbandsnr;
    ditem.m_outofbandsnr = outofbandsnr;
    ditem.m_decodeIndex = m_currentIndex;
    ditem.m_pDecoderIstance = pdecoder;
    m_currentDecodeItemMap.insert(pdecoder,ditem);
    m_respondedList << pdecoder;

    //check for presence in item groupings
    if (m_decodeItemGroupings.contains(name)) {
        //item group exists, check if this decoder has been added
        DecodeItemGroup* pgroup = m_decodeItemGroupings.value(name);
        //qWarning(DECODERCONTAINER) << " located item group ";
        if (pgroup->hasItem(ditem)) {
            //qWarning(DECODERCONTAINER) << " Decoder " << pmetadata->m_decoderName << " item already present " << name;
            //TODO: do nothing... or maybe kill the whole group?
        }
        else {
            //qWarning(DECODERCONTAINER) << " Adding item " << name << " for decoder " << pmetadata->m_decoderName;
            pgroup->addItem(ditem);
        }
    }
    else {
        //item group doesn't exist; create it
        //qWarning(DECODERCONTAINER) << "Creating new group for " << name;
        DecodeItemGroup* pgroup = new DecodeItemGroup();
        pgroup->addItem(ditem);
        m_decodeItemGroupings.insert(name, pgroup);
        //qWarning(DECODERCONTAINER) << "Inserted " << name;
    }

    //iterate through the keys and find completed lists
    QList<QString> m_digKeys = m_decodeItemGroupings.keys();
    for (int i = 0; i < m_digKeys.size(); i++) {
        DecodeItemGroup* pgroup = m_decodeItemGroupings.value(m_digKeys.at(i));

        //qWarning(DECODERCONTAINER) << "Group " << m_digKeys.at(i) << " size is " << pgroup->groupSize();
        //qWarning(DECODERCONTAINER) << "Decoder list size is " << m_decoderList.size();

        if (pgroup->groupSize() == m_decoderList.size()) {
            //qWarning(DECODERCONTAINER) << "All data in for " << m_digKeys.at(i);
            allDataIn(pgroup);

            m_decodeItemGroupings.remove(m_digKeys.at(i));
            delete pgroup;
        }
        else {
            pgroup->bumpAge();

            //TODO: check for timeout
        }
    }

//    if(m_currentDecodeItemMap.size() == m_decoderList.size())
//    {
//       qWarning(DECODERCONTAINER) << "ContainerTelemValueDecoded from decoder all data in for index " << QString::number(m_currentIndex);
//       m_decoderTimeout.stop();
//       allDataIn(false);
//    }
//    else
//    {
//       m_decoderTimeout.start(1000);
//    }
}


DecodeItem DecoderContainer::vote(DecodeItemGroup* itemGroup)
{
    DecodeItem referenceitem;
    //QList<DecodeItem> items = m_currentDecodeItemMap.values();

    referenceitem.m_pDecoderIstance = nullptr;

    qWarning(DECODERCONTAINER) << "inside vote...";

    for(int i = 0; i < itemGroup->groupSize(); i++)
    {
        if((referenceitem.m_pDecoderIstance == nullptr) && (itemGroup->m_decodedItems.at(i).m_status == DecodeStatus::NoErrors))
        {
            referenceitem = itemGroup->m_decodedItems.at(i);
            qWarning(DECODERCONTAINER) << "inside vote setting item no " << QString::number(i);
        }
        else
        {
            DecodeItem compareitem = itemGroup->m_decodedItems.at(i);
            bool status = compareitem.m_status == DecodeStatus::NoErrors;
            bool validated = validateItem(compareitem);
            bool conf = compareitem.m_confidence > referenceitem.m_confidence;
            bool snr = compareitem.m_inbandsnr > referenceitem.m_inbandsnr;
            bool snr1 = compareitem.m_outofbandsnr > referenceitem.m_outofbandsnr;

            if(status && validated)
            {
                if(conf && snr)
                {
                    referenceitem = compareitem;
                    qWarning(DECODERCONTAINER) << "inside vote setting item noA " << QString::number(i);
                }
                else
                {
                    if(conf)
                    {
                        referenceitem = compareitem;
                        qWarning(DECODERCONTAINER) << "inside vote setting item noB " << QString::number(i);
                    }
                }
            }
        }
    }

    return referenceitem;
}


void DecoderContainer::timerEvent()
{
    //qWarning(DECODERCONTAINER) << "inside timerEvent ...";
    //allDataIn(true);
}

void DecoderContainer::allDataIn(DecodeItemGroup* itemGroup)
{
    DecodeItem item = vote(itemGroup);
    Decoder* pdecoder = item.m_pDecoderIstance;

    //qWarning(DECODERCONTAINER) << "allDataIn ...";

    m_plastDecoderInUse = pdecoder;

    if(pdecoder != nullptr)
    {
        //DecodeItem item = m_currentDecodeItemMap.value(pdecoder);
        DecoderMetaData* pmetadata = m_metaDataMap.value(pdecoder);
        QString decoderdesc = "using decoder " + pmetadata->m_decoderName;

        qWarning(DECODERCONTAINER) << "allDataIn " << pmetadata->m_decoderName << " parameter name = " << item.m_name;
        emit telemValueDecoded(item.m_name, item.m_value, true, item.m_confidence, item.m_outofbandsnr, item.m_inbandsnr, item.m_status);

        m_metaDataMap.value(pdecoder)->bumpScore();

        QStringList names;
        QStringList values;
        QStringList inbandSNRs;
        QStringList outbandSNRs;
        QStringList statuses;
        QStringList confidences;
        QHash<QString,DecodeItem> itemmap;

        for (int i = 0; i < itemGroup->groupSize(); i++)
        {
            DecodeItem ditm = itemGroup->m_decodedItems.at(i);

            itemmap.insert(m_metaDataMap.value(ditm.m_pDecoderIstance)->m_decoderName, ditm);
        }

        QStringList keys = itemmap.keys();

        qSort(keys);
        qCCritical(DECODERCONTAINER) << "Keys = " << keys;


        for (int i = 0; i < keys.size(); i++)
        {
            DecodeItem ditm = itemmap.value(keys.at(i));

            names << ditm.m_name;
            values << QString::number(ditm.m_value);
            inbandSNRs << QString::number(ditm.m_inbandsnr);
            outbandSNRs << QString::number(ditm.m_outofbandsnr);
            statuses <<  statusDesc(ditm.m_status);
            confidences << QString::number(ditm.m_confidence);
        }

//        for(int i=0;i<m_decoderList.size();i++)
//        {
//            Decoder* pdec = m_decoderList.at(i);

//            if(m_currentDecodeItemMap.contains(pdec))
//            {
//                DecodeItem ditm = m_currentDecodeItemMap.value(pdec);

//                names << ditm.m_name;
//                values << QString::number(ditm.m_value);
//                inbandSNRs << QString::number(ditm.m_inbandsnr);
//                outbandSNRs << QString::number(ditm.m_outofbandsnr);
//                statuses <<  statusDesc(ditm.m_status);
//                confidences << QString::number(ditm.m_confidence);

//            }
//            else
//            {
//                names << "";
//                values << "";
//                inbandSNRs << "";
//                outbandSNRs << "";
//                statuses <<  "";
//                confidences << "";
//            }
//        }
        logEvent(pmetadata->m_decoderName, names.join("#"), values.join("#"), inbandSNRs.join("#"), outbandSNRs.join("#"), confidences.join("#"), statuses.join("#"));
    }

    m_currentDecodeItemMap.clear();
    m_respondedList.clear();

    writeFilterStats("filterstats.txt", m_filterStats);
}


bool DecoderContainer::validateItem(DecodeItem item)
{

    return true;
}


QStringList DecoderContainer::getFilters(QHash<QString,QString> properties)
{
    QStringList filterpaths;

    if(properties.contains(MULTIPLE_FILTER_lIST))
    {
        filterpaths = properties.value(MULTIPLE_FILTER_lIST).split(",");
    }

    return filterpaths;
}


QList<QHash<QString,QString>> DecoderContainer::getDecoderConfigs(QHash<QString,QString> properties)
{
    QList<QHash<QString,QString>> configurations;

    if(properties.contains(MULTIPLE_INSTANCE_CONFIGPATH))
    {
        QString path = properties.value(MULTIPLE_INSTANCE_CONFIGPATH);
        QString cfgfilepath = path + "/multiplecfg.cfg";
        QFile cfgfile(cfgfilepath);

        if(cfgfile.exists())
        {
            //qCInfo(DECODERCONTAINER) << "multiple instance config file found " << cfgfilepath;
            configurations = getMultipleConfigs(cfgfilepath).values();
        }
        else
        {
            //qCInfo(DECODERCONTAINER) << "multiple instance config file not found " << cfgfilepath;
            QDir dir(path);
            QStringList filters;

            filters << "*.csv" << "*.txt" << "*.flt";
            QStringList entries = dir.entryList(filters);

            //qCInfo(DECODERCONTAINER) << "found these filters " << entries;

            for(int i=0;i<entries.size();i++)
            {
//                QString filterpath = path + "/" + entries.at(i);
                QString filterpath = makeAbsolute(path, entries.at(i));
                QHash<QString,QString> map;

                map.insert(LOPASS_FILTER_PATH, filterpath);
                configurations << map;
            }
        }
    }
    else
    {   // in this case we will just specify filter paths if they exist
        QStringList filterpaths = getFilters(properties);

        for(int i=0;i<filterpaths.size();i++)
        {
            QString path = filterpaths.at(i);
            QHash<QString,QString> map;

            map.insert(LOPASS_FILTER_PATH, path);
            configurations << map;
        }

    }


    return configurations;
}

QHash<QString,QString> DecoderContainer::modifyProperties(QHash<QString,QString> originalprops, QHash<QString,QString> mods)
{
    QStringList keys = mods.keys();

    for(int i=0;i<keys.size();i++)
    {
        QString key = keys.at(i);

        originalprops.insert(key, mods.value(key));
    }

    return originalprops;
}

void DecoderContainer::logEvent(QString decoder, QString name, float value, float inbandsnr, float outofbandsnr, int confidence, QString status)
{
    QString logentry =  QDateTime::currentDateTime().toString() + "  Decoded Parameter " + name + " value = " + QString::number(value) + " inband SNR = " + QString::number(inbandsnr)
                                   + " out of band SNR = " + QString::number(outofbandsnr) + " confidence = " + QString::number(confidence) + " status = " + status;

    //qCWarning(DECODERCONTAINER) << "Decoded parameter " << name << " value = " << value << " inband SNR = " << inbandsnr
                           //  << " out of band SNR = " << outofbandsnr << " confidence = " << confidence;

    Utils::writeLogFile(m_logFileKey, logentry);

    if(m_logFileTableKey != -1)
    {
        QString parammsg = "%1,,,,,,%8,%2,%3,%4,%5,%6,%7";

        parammsg = parammsg.arg(QDateTime::currentDateTime().toString()).arg(name).arg(QString::number(value)).arg(QString::number(inbandsnr))
                .arg(QString::number(outofbandsnr)).arg(QString::number(confidence)).arg(status).arg(decoder);
        Utils::writeLogFile(m_logFileTableKey, parammsg);
    }
}

void DecoderContainer::logEvent(QString decoder, QString name, QString value, QString inbandsnr, QString outofbandsnr, QString confidence, QString status)
{
    QString logentry =  QDateTime::currentDateTime().toString() + "  Decoded Parameter " + name + " value = " + value + " inband SNR = " + inbandsnr
                                   + " out of band SNR = " + outofbandsnr + " confidence = " + confidence + " status = " + status;

    //qCWarning(DECODERCONTAINER) << "Decoded parameter " << name << " value = " << value << " inband SNR = " << inbandsnr
                            // << " out of band SNR = " << outofbandsnr << " confidence = " << confidence;

    Utils::writeLogFile(m_logFileKey, logentry);

    if(m_logFileTableKey != -1)
    {
        QString parammsg = "%1,,,,,,%8,%2,%3,%4,%5,%6,%7";

        parammsg = parammsg.arg(QDateTime::currentDateTime().toString()).arg(name).arg(value).arg(inbandsnr)
                .arg(outofbandsnr).arg(confidence).arg(status).arg(decoder);
        Utils::writeLogFile(m_logFileTableKey, parammsg);
    }
}


int DecoderContainer::getSamplesPerPoint()
{
    return 0;
}


QString DecoderContainer::statusDesc(DecodeStatus status)
{
    QString desc = "";

    switch(status)
    {
        case DecodeStatus::ECCError:
        desc = "ECC error";
        break;
    case DecodeStatus::NoErrors:
    desc = "";
    break;
    case DecodeStatus::ParityError:
    desc = "parity error";
    break;
    case DecodeStatus::Invalid_Data:
    desc = "Invalid data";
    break;
    case DecodeStatus::Invalid_No_Pulse:
    desc = "No Pulse";
    break;
    }

    return desc;
}


void DecoderContainer::flushAnnotations(float lastkey)
{
    QList<Decoder*> keys = m_annotationsCache.keys();

    for(int i=0;i<keys.size();i++)
    {
        QHash<float,DecoderAnnotation*> annotationmap = m_annotationsCache.value(keys.at(i));
        QList<float> fkeys = annotationmap.keys();

        qSort(fkeys);

        for(int j=0;j<fkeys.size();j++)
        {
            float key = fkeys.at(j);

            if(key <lastkey)
            {
                delete annotationmap.value(key);
                annotationmap.remove(key);
            }
        }

        m_annotationsCache.insert(keys.at(i), annotationmap);
    }
}


void DecoderContainer::writeFilterStats(QString path, QHash<unsigned int, QStringList> oldstats)
{
    QFile outputfile(path);
    bool status = outputfile.open(QFile::WriteOnly);
    QTextStream ostr(&outputfile);
    QStringList currentstats;

    qCWarning(DECODERCONTAINER) << "writing filter stats file status = " << status;

    QList<DecoderMetaData*> metalist = m_metaDataMap.values();

    for(int i=0;i<metalist.size();i++)
    {
        DecoderMetaData* pmetadata = metalist.at(i);
        QString statline = pmetadata->m_filterName + "," + QString::number(pmetadata->m_score);

        currentstats << statline;
    }

    oldstats.insert(m_decoderStartTime.toTime_t(), currentstats);

    QList<unsigned int> keylist = oldstats.keys();

    qSort(keylist);

    for(int i=0;i<keylist.size();i++)
    {
        unsigned int keyval = keylist.at(keylist.size()-1-i);
        QStringList stats = oldstats.value(keyval);

        ostr << "Date=" << QDateTime::fromTime_t(keyval).toString() << "\n";

        for(int j=0;j<stats.size();j++)
        {
            ostr <<  stats.at(j) << "\n";
        }

        ostr << "\n";
    }

    ostr.flush();
    outputfile.close();
}

QHash<unsigned int,QStringList> DecoderContainer::readFilterStats(QString path)
{
    QHash<unsigned int,QStringList> stats;
    QFile inputfile(path);
    bool status = inputfile.open(QFile::ReadOnly);
    QStringList currentstats;
    unsigned int currentdate = 0;

    while(status && !inputfile.atEnd())
    {
        QString inputline = inputfile.readLine().trimmed();

        if(!inputline.isEmpty())
        {
            if(inputline.contains("Date"))
            {
                if(currentdate != 0 && !currentstats.isEmpty())
                {
                    stats.insert(currentdate, currentstats);
                    currentstats.clear();
                }

                QString datestr = inputline.split("=").last();
                QDateTime date = QDateTime::fromString(datestr.trimmed());

                currentdate = date.toTime_t();
            }
            else
            {
                currentstats << inputline;
            }
        }
    }

    if(currentdate != 0 && !currentstats.isEmpty())
    {
        stats.insert(currentdate, currentstats);
        currentstats.clear();
    }

    return stats;
}


QHash<QString,QHash<QString,QString>> DecoderContainer::getMultipleConfigs(QString path)
{
    QHash<QString,QHash<QString,QString>> decoderconfigs;
    QFile inputfile(path);
    bool status = inputfile.open(QFile::ReadOnly);
    QHash<QString,QString> currentmap;
    QString currentname;

    qCInfo(DECODERCONTAINER) << "getMultipleConfigs() config file " << path << " opened with status " << status;

    while(!inputfile.atEnd())
    {
        QString line = inputfile.readLine().trimmed();

        if(!line.isEmpty() && line.contains("=") && !line.startsWith("#"))
        {
            QStringList parts = line.split("=");

            if(parts.size() == 2)
            {
                QString name = parts.first();
                QString value = parts.last();

                if(name.contains(DECODER_NAME))
                {
                    if(!currentmap.isEmpty() && !currentname.isEmpty())
                    {
                        decoderconfigs.insert(currentname, currentmap);
                        currentmap.clear();
                    }

                    currentname = value;
                }

                currentmap.insert(name, value);
            }
            else
            {
                qCWarning(DECODERCONTAINER) << "this line is not valid... " << line;
            }
        }

    }

    if(!currentmap.isEmpty() && !currentname.isEmpty())
    {
        decoderconfigs.insert(currentname, currentmap);
        currentmap.clear();
    }

    QStringList keys = decoderconfigs.keys();

    // iterate found configurations and make sure any filter paths are absolute
    for(int i=0;i<keys.size();i++)
    {
        QString key = keys.at(i);
        QHash<QString,QString> map = decoderconfigs.value(key);

        if(map.contains(LOPASS_FILTER_PATH))
        {
            QString filterpath = map.value(LOPASS_FILTER_PATH);
            QFileInfo info(path);

            filterpath = makeAbsolute(info.path(),filterpath);

            map.insert(LOPASS_FILTER_PATH, filterpath);
            decoderconfigs.insert(key,map);
        }
    }

    return decoderconfigs;
}


void DecoderContainer::dumpProperties(QHash<QString,QString> props)
{
    QStringList keys = props.keys();

    qSort(keys);
    qCInfo(DECODERCONTAINER) << "Dumping Properties... " << keys.size();

    for(int i=0;i<keys.size();i++)
    {
        QString keyvalue = keys.at(i);
        QString value = props.value(keyvalue);

        qCInfo(DECODERCONTAINER) << "   " << keyvalue << " = " << value;
    }


}

QString DecoderContainer::makeAbsolute(QString dirpath, QString filepath)
{
    QDir directory(dirpath);

    return QDir::isAbsolutePath(filepath) ? filepath:directory.absoluteFilePath(filepath);
}
