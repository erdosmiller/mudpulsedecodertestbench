#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qsettings.h>
#include <qdebug.h>
#include <qfiledialog.h>
#include "decoderproperties.h"
#include "decoder.h"
#include "decoders/decoderwarningcheck.h"
#include "filterblock.h"
#include "datasources/serialdatasource.h"
#include "decoderengines/decoderengineimpl.h"
#include "filterblocks/filterblockimpl.h"
#include "filters/medianbiasedfilter.h"
#include "decoders/decodercontainer.h"
#include <qloggingcategory.h>
#include "utils.h"

#define DOWNSAMPLETO "downsampleto"
#define DOWNSAMPLEFROM "downsamplefrom"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->tabWidget->setCurrentIndex(0);
    loadSettings();
    ui->progressBar->setRange(0,100);
    ui->progressBar->setValue(0);
    m_pointsPerFile = 0;
    ui->decodestop->setEnabled(false);
//    ui->rxdelaytime->setText("30");
//    ui->txdelaytime->setText("30");
//    ui->synccorrelatornegscaling->setText("0.2");
//    ui->minsyncpulseheight->setText("2");
    QLoggingCategory::setFilterRules("*.debug=false\n*.info=false\n*.warning=false\n*.critical=true" );
}

MainWindow::~MainWindow()
{
    saveSettings();
    delete ui;
}

void MainWindow::on_selectsourcefiles_clicked()
{
    QStringList paths = QFileDialog::getOpenFileNames(this, "Specify Input Files", m_sourceDir, "*.csv *.txt");

    if(!paths.isEmpty())
    {
        ui->sourcefiles->clear();
        m_sourcefiles = paths;

        for(int i=0;i<m_sourcefiles.size();i++)
        {
            QString fpath = m_sourcefiles.at(i);

            ui->sourcefiles->addItem(fpath.split("/").last());
        }

        ui->sourcefiles->setToolTip(m_sourcefiles.join("\n"));

        QHash<QString,QString> meta = loadFileMeta(m_sourcefiles.first());
        QStringList parts = m_sourcefiles.first().split("/");

        parts.removeLast();
        QString sourcepath = parts.join("/");

        m_sourceDir = sourcepath;


        if(meta.contains("samplerate"))
        {
            m_sampleFrequency = meta.value("samplerate");
            ui->samplerate->setText(m_sampleFrequency);
        }
        if(meta.contains("pulsewidth"))
        {
            m_pulseWidth = meta.value("pulsewidth");
            ui->pulsewidth->setText(m_pulseWidth);
        }

        if(meta.contains("ss1_flat_definition"))
        {
            m_surveySequence = meta.value("ss1_flat_definition");
            ui->surveysequence->setText(m_surveySequence);
            ui->surveysequence->setToolTip(m_surveySequence);
        }

        if(meta.contains("tfs1_flat_definition"))
        {
            m_toolfaceSequence = meta.value("tfs1_flat_definition");
            ui->toolfacesequence->setText(m_toolfaceSequence);
            ui->toolfacesequence->setToolTip(m_toolfaceSequence);
        }

        if(meta.contains("basefrequency"))
        {
            m_baseFrequency = meta.value("basefrequency");
            ui->basefrequency->setText(m_baseFrequency);
        }

        if(meta.contains("cyclespersymbol"))
        {
            m_cylcesPerSymbol = meta.value("cyclespersymbol");
            ui->cyclespersymbol->setText(m_cylcesPerSymbol);
        }

        QStringList sequencekeys;

        sequencekeys << SS1_FLAT_DEFINITION << SS2_FLAT_DEFINITION << SS3_FLAT_DEFINITION << SS4_FLAT_DEFINITION
                     << TFS1_FLAT_DEFINITION << TFS2_FLAT_DEFINITION << TFS3_FLAT_DEFINITION << TFS4_FLAT_DEFINITION;
        m_sequenceDefinitions.clear();

        for(int i=0;i<sequencekeys.size();i++)
        {
            QString key = sequencekeys.at(i);

            if(meta.contains(key))
            {
                m_sequenceDefinitions.insert(key, meta.value(key));
            }
        }

        ui->surveysequence->setToolTip(getSSQTooltips(m_sequenceDefinitions));
        ui->toolfacesequence->setToolTip(getTFSQTooltips(m_sequenceDefinitions));

    }
}

void MainWindow::on_selectmpfilter_clicked()
{
    QStringList parts = m_mpFilter.split("/");

    if(parts.size() > 1)
    {
        parts.removeLast();
    }

    QString path = QFileDialog::getOpenFileName(this, "Pick MudPulse filter", parts.join("/"), "Mud Pulse filters (*.csv *.flt *.txt *.*)");

    if(!path.isEmpty())
    {
        m_mpFilter = path;
        ui->mpfilter->setText(m_mpFilter.split("/").last());
        ui->mpfilter->setToolTip(m_mpFilter);
    }

}

void MainWindow::on_runmp_clicked()
{
    QHash<QString,QString> props = m_mpUseMultipleINstances ? getMPMultipleDecoderProps() : getMPDecoderProps();

    runDecoder(props);
}

void MainWindow::on_selectqpskfilter_clicked()
{
    QStringList parts = m_qpskFilter.split("/");

    if(parts.size() > 1)
    {
        parts.removeLast();
    }

    QString path = QFileDialog::getOpenFileName(this, "Pick QPSK filter", parts.join("/"), "QPSK filters (*.csv *.flt *.txt *.*)");

    if(!path.isEmpty())
    {
        m_qpskFilter = path;
        ui->qpskfilter->setText(m_qpskFilter.split("/").last());
        ui->qpskfilter->setToolTip(m_qpskFilter);
    }

}

void MainWindow::on_runqpsk_clicked()
{
    QHash<QString,QString> props = getQPSKDecoderProps();

    runDecoder(props);
}

void MainWindow::on_selectoutputdir_clicked()
{
    QString path = QFileDialog::getExistingDirectory(this, "Pick output Dir", m_outputDir);

    if(!path.isEmpty())
    {
        m_outputDir = path;
        ui->outputdir->setText(m_outputDir.split("/").last());
        ui->outputdir->setToolTip(m_outputDir);
    }

}


//QString m_surveySequence;
//QString m_toolfaceSequence;
//QString m_mpFilter;
//QString m_qpskFilter;
//bool m_useMPDiscriminator;
//QString m_pulseWidth;
//QString m_sampleFrequency;
//QString m_downsampleFrequency;
//QString m_outputDir;
//QString m_sourceDir;

void MainWindow::loadSettings()
{
    QSettings settings("settings.ini", QSettings::IniFormat, this);

    settings.beginGroup("General");
    QString sourcefiles = settings.value("sourcefiles", "").toString();

    m_surveySequence = settings.value("surveysequence", "").toString();
    m_toolfaceSequence = settings.value("toolfacesequence", "").toString();
    m_mpFilter = settings.value("mpfilter", "").toString();
    m_qpskFilter = settings.value("qpskfilter", "").toString();
    m_useMPDiscriminator = settings.value("usempdiscriminator", "false").toString().contains("true");
    m_pulseWidth = settings.value("pulsewidth", "").toString();
    m_sampleFrequency = settings.value("samplefrequency", "").toString();
    m_downsampleFrequency = settings.value("downsamplefrequency", "").toString();
    m_outputDir = settings.value("outputdir", "").toString();
    m_sourceDir = settings.value("sourcedir", "").toString();
    m_useDriftCorrection = settings.value("usedriftcorrection", "false").toString().contains("true");
    m_baseFrequency = settings.value("basefrequency", "").toString();
    m_cylcesPerSymbol = settings.value("cyclespersymbol", "").toString();
    m_mpUseMultipleINstances = settings.value("mpusemultipleinstances", "false").toString().contains("true");
    m_qpskUseMutipleInstances = settings.value("qpskusemultipleinstances", "false").toString().contains("true");
    m_stopOnLostSymbols = settings.value("stoponlostsymbols", "true").toString().contains("true");
    m_lostSymbolCount = settings.value("lostsymbolcount", "10").toString();
    m_currentTab = settings.value("currenttab", "0").toString().toInt();
    m_testFilterSampleRate = settings.value("testfiltersamplerate", "1000").toString();
    m_testFilterDownSampleRate = settings.value("testfilterdownsamplerate", "100").toString();
    m_testFilterPath = settings.value("testfilterpath", "").toString();
    m_testFilterOutPath = settings.value("testfilteroutputpath", "").toString();
    m_mpMultipleConfigurationPath = settings.value("mudpulseconfigpathmutiple", "").toString();
    m_emMultipleConfigurationPath = settings.value("emconfigpathmultiple", "").toString();
    m_rxDelayTime = settings.value("rxdelaytime", "30").toString();
    m_txDelayTime = settings.value("txdelaytime", "30").toString();
    m_negSyncScaling = settings.value("synccorrelationnegscaling", "0.2").toString();
    m_minSyncPulseHeight = settings.value("minsyncpulseheight", "2.0").toString();

    QString sequencedefs = settings.value("sequencedefinitions", "").toString();

    m_sequenceDefinitions = fromSequenceString(sequencedefs);
    qDebug() << "loaded sequence definitions " << m_sequenceDefinitions;

    settings.endGroup();

    ui->surveysequence->setText(m_surveySequence);
    ui->surveysequence->setToolTip(m_surveySequence);
    ui->toolfacesequence->setText(m_toolfaceSequence);
    ui->toolfacesequence->setToolTip(m_toolfaceSequence);
    ui->mpfilter->setText(m_mpFilter.split("/").last());
    ui->mpfilter->setToolTip(m_mpFilter);
    ui->qpskfilter->setText(m_qpskFilter.split("/").last());
    ui->qpskfilter->setToolTip(m_qpskFilter);
    ui->usemudpulsediscriminator->setChecked(m_useMPDiscriminator);
    ui->pulsewidth->setText(m_pulseWidth);
    ui->samplerate->setText(m_sampleFrequency);
    ui->outputdir->setText(m_outputDir.split("/").last());
    ui->outputdir->setToolTip(m_outputDir);
    ui->basefrequency->setText(m_baseFrequency);
    ui->cyclespersymbol->setText(m_cylcesPerSymbol);
    ui->trackdrift->setChecked(m_useDriftCorrection);
    ui->downsamplerate->setText(m_downsampleFrequency);
    ui->mpusemultipleinstances->setChecked(m_mpUseMultipleINstances);
    ui->qpskusemultipleinstances->setChecked(m_qpskUseMutipleInstances);
    ui->stoponlostsymbol->setChecked(m_stopOnLostSymbols);
    ui->lostsymbolcount->setText(m_lostSymbolCount);
    ui->tabWidget->setCurrentIndex(m_currentTab);
    ui->testfilterdownsmplerate->setText(m_testFilterDownSampleRate);
    ui->filtertestsamplerate->setText(m_testFilterSampleRate);
    ui->mp_mcfgpath->setText(m_mpMultipleConfigurationPath.split("/").last());
    ui->mp_mcfgpath->setToolTip(m_mpMultipleConfigurationPath);
    ui->em_mcfgpath->setText(m_emMultipleConfigurationPath.split("/").last());
    ui->em_mcfgpath->setToolTip(m_emMultipleConfigurationPath);

    QStringList sources = sourcefiles.split(",");

    for(int i=0;i<sources.size();i++)
    {
        ui->sourcefiles->addItem(sources.at(i).split("/").last());
        m_sourcefiles << sources.at(i);
    }

    ui->sourcefiles->setToolTip(sources.join("\n"));
    ui->surveysequence->setToolTip(getSSQTooltips(m_sequenceDefinitions));
    ui->toolfacesequence->setToolTip(getTFSQTooltips(m_sequenceDefinitions));
    ui->rxdelaytime->setText(m_rxDelayTime);
    ui->txdelaytime->setText(m_txDelayTime);
    ui->synccorrelatornegscaling->setText(m_negSyncScaling);
    ui->minsyncpulseheight->setText(m_minSyncPulseHeight);

}

void MainWindow::saveSettings()
{
    QSettings settings("settings.ini", QSettings::IniFormat, this);
    QString sourcefiles = m_sourcefiles.join(",");
    QString sequencedefs = toSequenceString(m_sequenceDefinitions);

    m_currentTab = ui->tabWidget->currentIndex();

    settings.beginGroup("General");
    settings.setValue("sourcefiles", sourcefiles);
    settings.setValue("surveysequence", m_surveySequence);
    settings.setValue("toolfacesequence", m_toolfaceSequence);
    settings.setValue("mpfilter", m_mpFilter);
    settings.setValue("qpskfilter", m_qpskFilter);
    settings.setValue("usempdiscriminator", (m_useMPDiscriminator?"true":"false"));
    settings.setValue("pulsewidth", m_pulseWidth);
    settings.setValue("samplefrequency", m_sampleFrequency);
    settings.setValue("downsamplefrequency", m_downsampleFrequency);
    settings.setValue("outputdir", m_outputDir);
    settings.setValue("sourcedir", m_sourceDir);
    settings.setValue("usedriftcorrection", (m_useDriftCorrection?"true":"false"));
    settings.setValue("basefrequency", m_baseFrequency);
    settings.setValue("cyclespersymbol", m_cylcesPerSymbol);
    settings.setValue("mpusemultipleinstances", (m_mpUseMultipleINstances?"true":"false"));
    settings.setValue("qpskusemultipleinstances", (m_qpskUseMutipleInstances?"true":"false"));
    settings.setValue("stoponlostsymbols", (m_stopOnLostSymbols?"true":"false"));
    settings.setValue("lostsymbolcount", m_lostSymbolCount);
    settings.setValue("currenttab", QString::number(m_currentTab));
    settings.setValue("testfiltersamplerate", m_testFilterSampleRate);
    settings.setValue("testfilterdownsamplerate", m_testFilterDownSampleRate);
    settings.setValue("testfilterpath", m_testFilterPath);
    settings.setValue("testfilteroutputpath", m_testFilterOutPath);
    settings.setValue("mudpulseconfigpathmutiple", m_mpMultipleConfigurationPath);
    settings.setValue("emconfigpathmultiple", m_emMultipleConfigurationPath);
    settings.setValue("sequencedefinitions", sequencedefs);
    settings.setValue("rxdelaytime", m_rxDelayTime);
    settings.setValue("txdelaytime", m_txDelayTime);
    settings.setValue("synccorrelationnegscaling", m_negSyncScaling);
    settings.setValue("minsyncpulseheight", m_minSyncPulseHeight);

    qDebug() << "saving these sequence definitions " << sequencedefs;

    settings.endGroup();
}

QString MainWindow::toSequenceString(QHash<QString,QString> sequencedefs)
{
    QStringList keylist = sequencedefs.keys();
    QStringList definitions;

    for(int i=0;i<keylist.size();i++)
    {
        QString key = keylist.at(i);
        QString def = sequencedefs.value(key);

        definitions << (key + "=" + def);
    }

    return definitions.join("#");
}

QHash<QString,QString> MainWindow::fromSequenceString(QString sequencedefs)
{
    QStringList definitions = sequencedefs.split("#");
    QHash<QString,QString> definitionsmap;

    for(int i=0;i<definitions.size();i++)
    {
        QString sub = definitions.at(i);
        QStringList parts = sub.split("=");
        QString key = parts.first();
        QString value = parts.last();

        definitionsmap.insert(key, value);
    }

    return definitionsmap;
}

QString MainWindow::getSSQTooltips(QHash<QString,QString> defs)
{
    QStringList keys;
    QStringList values;

    keys << SS1_FLAT_DEFINITION << SS2_FLAT_DEFINITION << SS3_FLAT_DEFINITION << SS4_FLAT_DEFINITION;

    for(int i=0;i<keys.size();i++)
    {
        QString keyval = keys.at(i);
        values << (defs.contains(keyval) ? defs.value(keyval):"");
    }

    return values.join("\n");
}


QString MainWindow::getTFSQTooltips(QHash<QString,QString> defs)
{
    QStringList keys;
    QStringList values;

    keys << TFS1_FLAT_DEFINITION << TFS2_FLAT_DEFINITION << TFS3_FLAT_DEFINITION << TFS4_FLAT_DEFINITION;

    for(int i=0;i<keys.size();i++)
    {
        QString keyval = keys.at(i);
        values << (defs.contains(keyval) ? defs.value(keyval):"");
    }

    return values.join("\n");
}


void MainWindow::on_samplerate_editingFinished()
{
    m_sampleFrequency = ui->samplerate->text().trimmed();
}

void MainWindow::on_pulsewidth_editingFinished()
{
    m_pulseWidth = ui->pulsewidth->text().trimmed();
}

void MainWindow::on_basefrequency_editingFinished()
{
    m_baseFrequency = ui->basefrequency->text().trimmed();
}

void MainWindow::on_cyclespersymbol_editingFinished()
{
    m_cylcesPerSymbol = ui->cyclespersymbol->text().trimmed();
}

void MainWindow::on_trackdrift_stateChanged(int arg1)
{
    m_useDriftCorrection = ui->trackdrift->isChecked();
}

void MainWindow::on_usemudpulsediscriminator_stateChanged(int arg1)
{
    m_useMPDiscriminator = ui->usemudpulsediscriminator->isChecked();
}

void MainWindow::on_downsamplerate_editingFinished()
{
    m_downsampleFrequency = ui->downsamplerate->text().trimmed();
}


QHash<QString,QString> MainWindow::loadFileMeta(QString filename)
{
    QHash<QString,QString> metadata;
    QFile inputfile(filename);
    bool status = inputfile.open(QFile::ReadOnly);
    bool pastheader = false;

    while(!inputfile.atEnd() && !pastheader)
    {
        QString inputstr = inputfile.readLine();

        if(inputstr.contains("="))
        {
            QStringList parts = inputstr.split("=");
            QString key = parts.first().trimmed();
            QString value = parts.last().trimmed();

            metadata.insert(key, value);
        }

        if(inputstr.contains("%%%%%%%%%"))
        {
            pastheader = true;
        }
    }

    return metadata;
}


QHash<QString,QString> MainWindow::getMPDecoderProps()
{
    QHash<QString,QString> properties;
    QString eventlogpath = m_outputDir.isEmpty() ? "events.csv" : m_outputDir + "/" + "events.csv";

    properties.insert(SS1_FLAT_DEFINITION, m_surveySequence);
    properties.insert(SS2_FLAT_DEFINITION, "ssq2 Inc:12:P Azm:12:P MagF:8:P DipA:10:P Grav:8:P Temp:6:P BatV:8:P");
    properties.insert(SS3_FLAT_DEFINITION, "ssq3 Inc:12:P Azm:12:P MagF:8:P DipA:10:P Grav:8:P Temp:6:P BatV:8:P");
    properties.insert(SS4_FLAT_DEFINITION, "ssq4 Inc:12:P Azm:12:P MagF:8:P DipA:10:P Grav:8:P Temp:6:P BatV:8:P");
    properties.insert(TFS1_FLAT_DEFINITION, m_toolfaceSequence);
    properties.insert(TFS2_FLAT_DEFINITION, "tsq2 atfa:6:P");
    properties.insert(TFS3_FLAT_DEFINITION, "tsq3 atfa:6:P");
    properties.insert(TFS4_FLAT_DEFINITION, "tsq4 atfa:6:P");

    properties.insert(PUMPS_ON_THRESHOLD, QString::number(400));
    properties.insert(SYNC_CORRELATION_THRESHOLD, QString::number(40));
    properties.insert(EVENT_LOG_FILE_PATH, eventlogpath);
    properties.insert(SEND_POINTS, "true");
    properties.insert(RESYNC_FOR_TOOLFACE, "false");
    properties.insert(LOPASS_FILTER_STAGES, "1");
    properties.insert(HIPASS_FILTER_PATH, "C:/tpgwork/mudpulsetesting/decoderlib/em-decoder/MudPulse/decoderlib/support/highpass_3Hz.flt");
    properties.insert(MEDIAN_FILTER_BIAS, "0.1");
//    properties.insert(MEDIAN_FILTER_PULSE_SCALING, "8");
//    properties.insert(MEDIAN_FILTER_WINDOW_DEPTH, "2000");           // !!! experimental
//    properties.insert(FILTER_USE_PULSE_DISCRIMINATION, "true");
//    properties.insert(FILTER_MIN_PULSE_WIDTH, "2");
//    properties.insert(FILTER_MIN_PULSE_WIDTH_TOLERANCE, "0.75");
//    properties.insert(FILTER_MIN_PULSE_SEPARATION, "5");
//    properties.insert(FILTER_MIN_PULSE_SEPARATION_TOLERANCE, "0.90");
//    properties.insert(FILTER_MIN_PULSE_AMPLITUDE, "12.0");
    properties.insert(DECODER_ALWAYS_LOOK_FOR_SYNC, "true");
//    properties.insert(DECODER_TYPE, DECODER_MUDPULSE);
//    properties.insert(PACKET_AGGREGATOR_TYPE, PACKET_AGGREGATOR_DEFAULT);

   if(m_useMPDiscriminator)
   {
       properties.insert(FILTER_USE_PULSE_DISCRIMINATION, "true");
   }

    properties.insert(DOWNSAMPLEFROM, ui->samplerate->text());
    properties.insert(ORIGINAL_SAMPLE_RATE, ui->samplerate->text());
    properties.insert(ACTUAL_SAMPLE_RATE, ui->samplerate->text());
    properties.insert(RAW_SAMPLE_RATE, ui->downsamplerate->text());
    properties.insert(DOWNSAMPLETO, ui->downsamplerate->text());

    properties.insert(PULSE_WIDTH, ui->pulsewidth->text());
    properties.insert(STATSFILE, "statsfile.csv");
//    properties.insert(IGNORE_PUMPS, "true");
    properties.insert(SYNC_CORRELATION_THRESHOLD, QString::number(0.4));
    properties.insert(LOPASS_FILTER_PATH, "C:/tpgwork/mudpulsetesting/decoderlib/em-decoder/MudPulse/decoderlib/support/fir_1hz_lopass_1000.flt");
    properties.insert(PUMPS_ON_THRESHOLD, QString::number(100));
    properties.insert(IGNORE_PUMPS, "true");
    properties.insert(DECODER_ALWAYS_LOOK_FOR_SYNC, "false");
    properties.insert(STOP_ON_LOST_SIGNAL, "true");
    properties.insert(MIN_SNR, "3.0");
    properties.insert(DROPOUT_SYMBOL_COUNT, "10");
    properties.insert(LOPASS_FILTER_PATH, m_mpFilter);


    properties.insert(RAWDATA_CHUNK_SIZE, ui->samplerate->text());
    properties.insert(CORRECT_FOR_DRIFT, (m_useDriftCorrection ? "true":"false"));
    properties.insert(SYNC_CORRELATION_THRESHOLD, QString::number(0.2));
    properties.insert(IGNORE_PUMPS, "false");
    properties.insert(PUMPS_ON_THRESHOLD, QString::number(500));
    properties.insert(STOP_ON_LOST_SIGNAL,(m_stopOnLostSymbols?"true":"false"));
    properties.insert(DROPOUT_SYMBOL_COUNT, m_lostSymbolCount);

    properties.insert(SYNC_CORRELATION_NEG_SCALING, ui->synccorrelatornegscaling->text());
    properties.insert(RX_DELAY_TIME, ui->rxdelaytime->text());
    properties.insert(TX_DELAY_TIME, ui->txdelaytime->text());
    properties.insert(SYNC_MIN_PULSE_THRESHOLD, ui->minsyncpulseheight->text());

    QStringList keys = m_sequenceDefinitions.keys();

    for(int i=0;i<keys.size();i++)
    {
        QString propkey = keys.at(i);
        properties.insert(propkey, m_sequenceDefinitions.value(propkey));
    }


    return properties;
}

QHash<QString,QString> MainWindow::getMPMultipleDecoderProps()
{
    QHash<QString,QString> props = getMPDecoderProps();
//    QString paths = "path1,path2,path3";      // not real paths replace them

//    props.insert(MULTIPLE_FILTER_lIST, paths);
    // possibly add multiple filter paths here instead....
    props.insert(MULTIPLE_INSTANCE_CONFIGPATH, m_mpMultipleConfigurationPath);


    return props;
}

QHash<QString,QString> MainWindow::getQPSKDecoderProps()
{
    QHash<QString,QString> props;

    return props;
}


void MainWindow::runDecoder(QHash<QString,QString> props)
{
    long count = 0;

    ui->decodestop->setEnabled(true);
    m_cachedFiles = m_sourcefiles;
    m_cachedPoints.clear();
    m_pointsPerFile = 0;
    ui->progressBar->setValue(0);
    m_abortDecode = false;

    qDebug() << "Starting decoder... processing this many files " << m_cachedFiles.size();
    dumpProperties(props);

    Decoder* dec = nullptr;
    FilterBlock* pfilters = getFilterBlock(props);      // this serves no purpose - need to rework...
    SerialDataSource* pdatasrc = new TestDataSource();

    if(m_mpUseMultipleINstances)
    {
        QList<QHash<QString,QString>> decoderconfigs = DecoderContainer::getDecoderConfigs(props); // this seems a little excessive...
        QList<Decoder*> decoderlist;
        QList<FilterBlock*> filterblocklist;

        dec = new DecoderContainer(new SequenceParser());

        for(int i=0;i<decoderconfigs.size();i++)
        {
            DecoderWarningCheck* pdecoder = new DecoderWarningCheck(new SequenceParser);

            pdecoder->setSymbolDecoder(DecoderEngineImpl::getSymbolDecoder(props, pdecoder));
            pdecoder->setPacketAggregator(DecoderEngineImpl::getAggregator(props));

            decoderlist << pdecoder;
            filterblocklist << new FilterBlockImpl();
        }

        ((DecoderContainer*)dec)->setDecoders(decoderlist, filterblocklist, pdatasrc);
    }
    else
    {
        dec = new DecoderWarningCheck(new SequenceParser);
    }

    DecoderEngineImpl* pdecoderengine = new DecoderEngineImpl(pfilters,pdatasrc,dec);

    connect(dec,SIGNAL(filteredDataAvailable(QList<QPointF>, QList<QPointF>, QList<QPointF>, QHash<float,DecoderAnnotation*>)), this, SLOT(filteredDataAvailable(QList<QPointF>, QList<QPointF>, QList<QPointF>, QHash<float,DecoderAnnotation*>)));

    if(!m_mpUseMultipleINstances)
    {
        ((DecoderWarningCheck*)dec)->setSymbolDecoder(pdecoderengine->getSymbolDecoder(props, dec));
        ((DecoderWarningCheck*)dec)->setPacketAggregator(pdecoderengine->getAggregator(props));
    }

    pdecoderengine->initialize(props);
    m_decodedData.clear();

    while(true && haveMorePoints())
    {
       count++;
//       nextPoint();
       pdatasrc->addData(nextPoint());

       if(count%50 == 0)
       {
           QApplication::processEvents();
       }

       if(m_pointsPerFile > 0)
       {
           int totalpoints = m_pointsPerFile * m_sourcefiles.size();
           int percent = ((float)count/(float)totalpoints) * 100;

           ui->progressBar->setValue(percent);
       }

       if(m_abortDecode)
       {
           break;
       }
    }

    QString outpath = m_outputDir.isEmpty() ? "decodeddata.csv" : m_outputDir + "/" + "decodeddata.csv";

    ui->progressBar->setValue(100);

    writeProcessedData(outpath);
//    writePumpStateData("pumpstate.csv");
    delete pdecoderengine;
    qDebug() << "!!!!! finished processing this many points " << count;
    ui->decodestop->setEnabled(false);
    m_cachedPoints.clear();
    m_decodedData.clear();
}

FilterBlock* MainWindow::getFilterBlock(QHash<QString,QString> props)
{
    FilterBlock* pblock = nullptr;

    pblock = new FilterBlockImpl();
    return pblock;
}


QList<float>  MainWindow::loadPoints(QStringList filenames)
{
    QList<float> points;

    for(int i=0;i<filenames.size();i++)
    {
        QString fname = filenames.at(i);

        points.append(loadFile(fname));
    }

    qDebug() << "loaded files using this many points " << points.size();
    return points;
}

QList<float> MainWindow::loadFile(QString filename)
{
    QList<float> points;
    QFile inputfile(filename);
    bool status = inputfile.open(QFile::ReadOnly);
    bool pastheader = false;


    qDebug() << "Opened file " << filename << " with status " <<  status;

    while(!inputfile.atEnd())
    {
        QString inputline = inputfile.readLine();

        if(pastheader)
        {
            if(!inputline.contains("Pressure"))
            {
                if(!inputline.contains(","))
                {
                    float value = inputline.trimmed().toFloat();

                    points << value;
                }
                else
                {
                    QStringList parts = inputline.split(",");
                    float value = parts.last().trimmed().toFloat();

                    points << value;
                }
            }
        }
        else
        {
            if(inputline.contains("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%") || inputline.contains("---------------"))
            {
                pastheader = true;
            }
        }
    }

    if(m_pointsPerFile == 0)
    {
        m_pointsPerFile = points.size();
    }

    qDebug() << "Opened file " << filename << " returning this many points " << points.size();
    return points;
}


bool MainWindow::haveMorePoints()
{
    return !m_cachedPoints.isEmpty() || !m_cachedFiles.isEmpty();
}

float MainWindow::nextPoint()
{
    float point = 0;

    if(!m_cachedPoints.isEmpty())
    {
        point = m_cachedPoints.dequeue();
    }
    else
    {
        int filestoload = m_cachedFiles.size()>5?5:m_cachedFiles.size();
        QStringList templist;

        qDebug() << "Caching  loading this many files " << QString::number(filestoload);

        for(int i=0;i<filestoload;i++)
        {
            templist << m_cachedFiles.first();
            m_cachedFiles.removeFirst();
        }

        QList<float> points = loadPoints(templist);

        for(int i=0;i<points.size();i++)
        {
            m_cachedPoints.enqueue(points.at(i));
        }

        qDebug() << "loaded this many points " << QString::number(m_cachedPoints.size());
    }

    return point;
}


void MainWindow::cachePoints(QStringList paths)
{
    m_cachedFiles = paths;

    int filestoload = m_cachedFiles.size()>5?5:m_cachedFiles.size();
    QStringList templist;

    qDebug() << "Caching data from this many files " << QString::number(paths.size()) << " loading this many " << QString::number(filestoload);

    for(int i=0;i<filestoload;i++)
    {
        templist << m_cachedFiles.first();
        m_cachedFiles.removeFirst();
    }

    QList<float> points = loadPoints(templist);

    for(int i=0;i<points.size();i++)
    {
        m_cachedPoints.enqueue(points.at(i));
//        qDebug() << "cache - loading this point " << QString::number(points.at(i));
    }

    qDebug() << "loaded this many points " << QString::number(m_cachedPoints.size());
}



void MainWindow::writeProcessedData(QString filename)
{
    QFile outfile(filename);
    bool status = outfile.open(QFile::WriteOnly);
    QTextStream strm(&outfile);

    strm << "time,raw,filtered,ticks,parameter,symbolvalue\n";

    for(int i=0;i<m_decodedData.size();i++)
    {
        QHash<QString,QString> rowdata = m_decodedData.at(i);
        QStringList row;
        QString index = QString::number(i);
        QString raw = rowdata.value("raw");
        QString filtered = rowdata.value("filtered");
        QString ticks = rowdata.value("ticks");
        QString param = rowdata.value("parametername");
        QString symbol = rowdata.value("symbolvalue");

        row << index << raw << filtered << ticks << param << symbol;
        strm << row.join(",") << "\n";
    }


    m_decodedData.clear();
}

void MainWindow::on_decodestop_clicked()
{
    m_abortDecode = true;
}


void MainWindow::dumpProperties(QHash<QString,QString> props)
{
    QStringList keys = props.keys();

    qSort(keys);
    qDebug() << "Dumping Properties... " << keys.size();

    for(int i=0;i<keys.size();i++)
    {
        QString keyvalue = keys.at(i);
        QString value = props.value(keyvalue);

        qDebug() << "   " << keyvalue << " = " << value;
    }


}


void MainWindow::filteredDataAvailable(QList<QPointF> raw, QList<QPointF> filtered, QList<QPointF> pressure, QHash<float,DecoderAnnotation*> annotations)
{
//    qDebug() << "raw points " << raw.size() << " filtered points " << filtered.size();

//    QList<DecoderAnnotation*> alist = annotations.values();
//    for(int i=0;i<alist.size();i++)
//    {
//        delete alist.at(i);
//    }

//    return;
    for(int i=0;i<filtered.size();i++)
    {
        QHash<QString,QString> rowdata;

        rowdata.insert("filtered", QString::number(filtered.at(i).y()));
        rowdata.insert("raw", QString::number(raw.at(i).y()));
        rowdata.insert("ticks", QString::number(0));
        rowdata.insert("parametername", "");
        rowdata.insert("symbolvalue", "");
        m_decodedData <<  rowdata;

    }

    QList<float> cachedkeys = m_cachedAnnotations.keys();

    for(int i=0;i<cachedkeys.size();i++)
    {
        float key = cachedkeys.at(i);

        annotations.insert(key, m_cachedAnnotations.value(key));
    }

    m_cachedAnnotations.clear();

    QList<float> keys = annotations.keys();


    for(int j=0;j<keys.size();j++)
    {
        int keyvalue = keys.at(j);

        if(m_decodedData.size() > keyvalue && annotations.contains(keyvalue))
        {
//                qDebug() << "annotation key = " <<  keys.at(j) << " key val = " << keyvalue << " total rows = " << m_decodedData.size();
            DecoderAnnotation* pann = annotations.value(keyvalue);

            QHash<QString,QString> rowdata = m_decodedData.at(keyvalue);

            if(pann->m_type == DecoderAnnotationType::SyncMarker)
            {
                float rowval = rowdata.value("ticks").toFloat();

                rowval += 20;
                rowdata.insert("ticks", QString::number(rowval));
                rowdata.insert("parametername", "Sync Marker");
                m_decodedData.replace(keyvalue, rowdata);
            }

            if(pann->m_type == DecoderAnnotationType::TelemetryValue)
            {
                float rowval = rowdata.value("ticks").toFloat();

                if(pann->m_name.contains("Drift"))
                {
                    if(pann->m_name.contains("Large"))
                    {
                        rowval += 15;
                    }
                    else
                    {
                        rowval += 10;
                    }
                }
                else
                {
                    rowval += 5;
                }

                rowdata.insert("ticks", QString::number(rowval));
                rowdata.insert("parametername", pann->m_name + ":" + QString::number(pann->m_value) + ":" + QString::number(pann->m_convertedValue));
                m_decodedData.replace(keyvalue, rowdata);
            }
            else
            {
                if(pann->m_type == DecoderAnnotationType::PacketMarker)
                {
                    float rowval = rowdata.value("ticks").toFloat();

                    rowval += 3;

                    rowdata.insert("ticks", QString::number(rowval));
                    m_decodedData.replace(keyvalue, rowdata);
                }
                else
                {
                    if(true && pann->m_type == DecoderAnnotationType::TUMarker)
                    {
                        float rowval = rowdata.value("ticks").toFloat();

                        rowval += 1;

                        rowdata.insert("ticks", QString::number(rowval));
                        m_decodedData.replace(keyvalue, rowdata);
                    }
                }

            }

            if(pann->m_type == DecoderAnnotationType::SymbolValue)
            {
                rowdata.insert("symbolvalue", QString::number(pann->m_value));
                m_decodedData.replace(keyvalue, rowdata);
            }

            delete pann;
        }
        else
        {
            DecoderAnnotation* pann = annotations.value(keyvalue);

            qDebug() << "skipping this annotation " << pann->m_type << " name = " << pann->m_name << " key = " << keyvalue << " data len = " << m_decodedData.size();
            m_cachedAnnotations.insert(keyvalue, pann);
        }
    }
}








void MainWindow::mergeRawFiles(QStringList sourcefiles, QString target)
{
    QFile outfile(target);
    bool outstatus = outfile.open(QFile::WriteOnly);
    QTextStream ostrm(&outfile);
    bool headerwritten = false;
    long index = 0;
    long totalwritten = 0;

    qDebug() << "opened output file named " <<  target << " status = " << outstatus;

    for(int i=0;i<sourcefiles.size();i++)
    {
        QString infilename = sourcefiles.at(i);
        QFile infile(infilename);
        bool instatus = infile.open(QFile::ReadOnly);
        bool pastheader = false;

        qDebug() << "opened this file " << infilename << " status " << instatus;

        while(!infile.atEnd())
        {
            QString inputline = infile.readLine().trimmed();

            if(pastheader || !headerwritten)
            {
                if(!headerwritten)
                {
                    if(!inputline.isEmpty())
                    {
                        ostrm << inputline << "\n";
                    }
                }

                if(pastheader)
                {
//                    if((index++ % 10) == 0)
                    if((index++ % 8) == 0)
                    {
                        if(!inputline.isEmpty())
                        {
                            totalwritten++;

                            if(inputline.contains(","))
                            {
                                inputline = inputline.split(",").last();
                            }

                            ostrm << inputline << "\n";
                        }
                    }
                }



            }

            if(inputline.contains("Pressure"))
            {
                pastheader = true;
                headerwritten = true;
            }

        }
    }

    qDebug() << "file " << target << " written " << " total lines " << index << " wirtten " << totalwritten;

}

QList<QHash<QString,QList<QList<QHash<QString, QString>>>>>  MainWindow::parseSurveys(QString filepath)
{
    QList<QList<QHash<QString,QString>>> surveys;
    QList<QHash<QString, QString>> fileinput = readSurveyFile(filepath);
    QList<QHash<QString, QString>> buffer;
    QList<QHash<QString, QString>> currentitem;
    QHash<QString,QList<QList<QHash<QString, QString>>>> paramshash; // a map to a list containing a list of hashes for a partiular parameter
                                                                     // contains all the points in a survey/toolface run including the sync marker
    QList<QHash<QString,QList<QList<QHash<QString, QString>>>>> surveyslist;  // a list containing all the survey points found
    bool lookingforsync = true;


    for(int i=0;i<fileinput.size();i++)
    {
        QHash<QString,QString> linehash = fileinput.at(i);

        if(lookingforsync && linehash.contains("ticks"))
        {
            int tickvalue = linehash.value("ticks").toInt();

            if(!paramshash.isEmpty())
            {
                surveyslist << paramshash;
                paramshash.clear();
            }

            if(tickvalue == 1)
            {
                qDebug() << "starting a sync run";

                currentitem.clear();

                for(int j=0;j<buffer.size();j++)
                {
                    currentitem << buffer.at(j);
                }

                buffer.clear();
                lookingforsync = false;
                currentitem << linehash;

            }
        }

        if(!lookingforsync)
        {
            currentitem << linehash;

            if(linehash.contains("parameter"))
            {
                QString paramname = linehash.value("parameter").trimmed();

                if(!paramname.isEmpty())
                {
                    QList<QList<QHash<QString, QString>>> paramruns;
                    QStringList parts = paramname.split(":");

                    QString name = parts.first();
                    QString msg = (parts.size() == 3) ? ("Name: " + name + " Raw Vaue = " + parts.at(1) + " Converted Value = " + parts.last()) : ("Name: " + name);
                    qDebug() << "found parameter named " << name << " values =  " << paramname << " current size = " << currentitem.size();

                    if(paramshash.contains(name))
                    {
                        paramruns = paramshash.value(name);
                    }

                    QHash<QString, QString> firstrow = currentitem.first();

                    firstrow.insert("description", msg);
                    currentitem.replace(0, firstrow);

                    paramruns << currentitem;
                    paramshash.insert(name,paramruns);
                    currentitem.clear();
                    currentitem << buffer; // populate the new current item with a few points for continuity...
                    currentitem << linehash; // insert this line
                    buffer.clear();
                }
            }
        }

        if(currentitem.size() > 700)    // this number is a rough guess and will depend on resolution...
        {
            lookingforsync = true;
            currentitem.clear();
            qDebug() << "setting looking for sync to true dumping cache";
        }

//        if(lookingforsync)
        {
            buffer << fileinput.at(i);

            if(buffer.size() > 200)
            {
                buffer.removeFirst();
            }
        }
    }

    if(!paramshash.isEmpty())
    {
        surveyslist << paramshash;
        paramshash.clear();
    }

    return surveyslist;
}


QList<QHash<QString, QString>> MainWindow::readSurveyFile(QString path)
{
    QList<QHash<QString, QString>> filecontent;
    QFile inputfile(path);
    bool status = inputfile.open(QFile::ReadOnly);
    QString header = inputfile.readLine().trimmed();
    QStringList headerlist = header.split(",", QString::SkipEmptyParts);

    qDebug() << "opened file " << path << " with status " << status << " header = " << header;

    while(!inputfile.atEnd())
    {
        QString fileline = inputfile.readLine().trimmed();
        QStringList parts = fileline.split(",", QString::KeepEmptyParts);
        QHash<QString,QString> linemap;

        for(int i=0;i<headerlist.size();i++)
        {
            QString subpart = i < parts.size() ? parts.at(i) : "";
            QString key = headerlist.at(i);

            linemap.insert(key, subpart);
        }

        filecontent << linemap;
    }

    return filecontent;
}

void MainWindow::splitSurveyFile(QString inputfile, QString outputdir)
{
    QList<QHash<QString,QList<QList<QHash<QString, QString>>>>> surveyslist = parseSurveys(inputfile);
    QDir dir;

    qDebug() << "got this many discrete surveys " << surveyslist.size();

    dir.mkdir(outputdir);

    QStringList headervalues = QString("time,raw,filtered,ticks,parameter,symbolvalue,description").split(",");

    for(int i=0;i<surveyslist.size();i++)
    {
        QHash<QString,QList<QList<QHash<QString, QString>>>> surveycontentmap = surveyslist.at(i);
        QStringList keys = surveycontentmap.keys();
        QString surveydir = outputdir + "/" + "Survey_" + QString::number(i);
        QDir sdir;

        sdir.mkdir(surveydir);

        qDebug() << "Survey " <<  i << " got this many named items " << keys.size();

        for(int j=0;j<keys.size();j++)
        {
            QString keyvalue = keys.at(j);
            QList<QList<QHash<QString, QString>>> parameterslist = surveycontentmap.value(keyvalue);

            qDebug() << "key value = " << keyvalue << " item count = " << parameterslist.size();

            for(int k=0;k<parameterslist.size();k++)
            {
                QList<QHash<QString, QString>> paramfilecontents = parameterslist.at(k);
                QString decoratedkey = keyvalue + "_" + QString::number(k);
                QString filepath = surveydir + "/" + decoratedkey + ".csv";

                qDebug() << "for item " << k << " row count = " << paramfilecontents.size();
                writeSurveyFile(paramfilecontents, filepath, headervalues);
            }

        }
    }
}


void MainWindow::writeSurveyFile(QList<QHash<QString, QString> > surveydata, QString path, QStringList headers)
{
    QFile outputfile(path);
    bool status = outputfile.open(QFile::WriteOnly);
    QTextStream str(&outputfile);

    qDebug() << "writing this file " << path << " status = " << status;

    str << headers.join(",") << "\n";

    for(int i=0;i<surveydata.size();i++)
    {
        QHash<QString, QString> linemap = surveydata.at(i);
        QStringList itemlist;

        for(int j=0;j<headers.size();j++)
        {
            QString item;
            QString key = headers.at(j);

            if(linemap.contains(key))
            {
                item = linemap.value(key);
            }

            itemlist << item;
        }

        str << itemlist.join(",") << "\n";
    }
}

void MainWindow::on_splitdecodes_clicked()
{
    QString path = QFileDialog::getOpenFileName(this, "Pick Survey File", m_outputDir, "*.csv");

    if(!path.isEmpty())
    {
        QString dirpath = QFileDialog::getExistingDirectory(this, "Pick a Target Directory", m_outputDir);

        if(!dirpath.isEmpty())
        {
            splitSurveyFile(path, dirpath);
        }
    }
}

void MainWindow::on_mergeraw_clicked()
{
    QStringList inputfiles = QFileDialog::getOpenFileNames(this, "Pick Raw files", m_sourceDir, "*.csv *.txt");

    if(!inputfiles.isEmpty())
    {
        QString firstfilename = inputfiles.first().split("/").last();

        qDebug() << "merged file = " << firstfilename;
        firstfilename = m_outputDir + "/" + firstfilename;
        mergeRawFiles(inputfiles, firstfilename);

    }
}

void MainWindow::on_stoponlostsymbol_stateChanged(int arg1)
{
    m_stopOnLostSymbols = ui->stoponlostsymbol->isChecked();
}

void MainWindow::on_lostsymbolcount_editingFinished()
{
    m_lostSymbolCount = ui->lostsymbolcount->text().trimmed();
}

void MainWindow::on_mpusemultipleinstances_stateChanged(int arg1)
{
    m_mpUseMultipleINstances = ui->mpusemultipleinstances->isChecked();
}

void MainWindow::on_qpskusemultipleinstances_stateChanged(int arg1)
{
    m_qpskUseMutipleInstances = ui->qpskusemultipleinstances->isChecked();
}

void MainWindow::on_testfilter_clicked()
{
    QStringList paths = QFileDialog::getOpenFileNames(this, "Specify Input Files", m_sourceDir, "*.csv *.txt");

    if(!paths.isEmpty())
    {
        QStringList parts = m_testFilterPath.split("/");

        if(parts.size() > 1)
        {
            parts.removeLast();
        }

        QString filterpath = QFileDialog::getOpenFileName(this, "Pick Test filter", parts.join("/"), "Filters (*.csv *.flt *.txt *.*)");

        if(!filterpath.isEmpty())
        {
            QStringList outparts = m_testFilterOutPath.split("/");

            if(outparts.size() > 1)
            {
                outparts.removeLast();
            }

            QString outpath = QFileDialog::getSaveFileName(this, "Specify output file", outparts.join("/"), "*.csv");

            if(!outpath.isEmpty())
            {
                m_testFilterOutPath = outpath;
                m_testFilterPath = filterpath;

                testSimpleFilters(paths, m_testFilterPath, outpath);
            }
        }

    }

}

void MainWindow::on_filtertestsamplerate_editingFinished()
{
    m_testFilterSampleRate = ui->filtertestsamplerate->text().trimmed();
}

void MainWindow::on_testfilterdownsmplerate_editingFinished()
{
    m_testFilterDownSampleRate = ui->testfilterdownsmplerate->text().trimmed();
}

void MainWindow::on_em_mcfgdirselect_clicked()
{
    QString path = QFileDialog::getExistingDirectory(this, "Pick EM configuration dir", m_emMultipleConfigurationPath);

    if(!path.isEmpty())
    {
        m_emMultipleConfigurationPath = path;
        ui->em_mcfgpath->setText(m_emMultipleConfigurationPath.split("/").last());
        ui->em_mcfgpath->setToolTip(m_emMultipleConfigurationPath);
    }

}

void MainWindow::on_mp_mcfgdirselect_clicked()
{
    QString path = QFileDialog::getExistingDirectory(this, "Pick MP configuration dir", m_mpMultipleConfigurationPath);

    if(!path.isEmpty())
    {
        m_mpMultipleConfigurationPath = path;
        ui->mp_mcfgpath->setText(m_mpMultipleConfigurationPath.split("/").last());
        ui->mp_mcfgpath->setToolTip(m_mpMultipleConfigurationPath);
    }

}

void MainWindow::on_pumpcycles_clicked()
{
    QStringList paths = QFileDialog::getOpenFileNames(this, "Specify Raw input Files", m_sourceDir, "*.csv");

    if(!paths.isEmpty())
    {
        QString path = QFileDialog::getExistingDirectory(this, "Pick Pump Cycle dir", "");
        QQueue<QString> queuedfilelines;


        if(!path.isEmpty())
        {
            QStringList headerlines;
            bool headerspopulated = false;
            bool inpumpcycle = false;
            bool resetpumpcycle = true;
            int pumpcycle_count = 0;
            int pumpcylelen = 0;
            QFile* outputfile = nullptr;
            QTextStream* ostr = nullptr;

            while(!paths.isEmpty())
            {
                QString filepath = paths.first();
                QFile inputfile(filepath);
                bool status = inputfile.open(QFile::ReadOnly);
                bool pastheader = false;

                qDebug() << "reading input file " << filepath << " status = " << status;

                paths.removeFirst();

                while(!inputfile.atEnd())
                {
                    QString inputline = inputfile.readLine().trimmed();

                    if(!pastheader && !headerspopulated)
                    {
                        headerlines << inputline;
                    }

                    if(pastheader && !inputline.startsWith("Pressure"))
                    {
                        if(queuedfilelines.size() < 500)
                        {
                            queuedfilelines.enqueue(inputline);
                        }
                        else
                         {
                            float pressure = inputline.toFloat();

                            if(!inpumpcycle)
                            {
                                if(resetpumpcycle)
                                {
                                    inpumpcycle = pressure > 500.0;

                                    if(inpumpcycle)
                                    {
                                        resetpumpcycle = false;
                                    }
                                }
                                else
                                {
                                    resetpumpcycle = pressure < 400.0;
                                }


                                if(inpumpcycle)
                                {
                                    qDebug() << "inside pump cycle...";

                                    if(outputfile != nullptr)
                                    {
                                        ostr->flush();
                                        outputfile->close();
                                        delete ostr;
                                        delete outputfile;
                                        ostr = nullptr;
                                        outputfile = nullptr;
                                    }

                                    QString outfilename = "pumpcycle_" + QString::number(pumpcycle_count++) + ".csv";

                                    pumpcylelen = 0;
                                    outfilename = path + "/" + outfilename;
                                    outputfile = new QFile(outfilename);

                                    bool status = outputfile->open(QFile::WriteOnly);

                                    ostr = new QTextStream(outputfile);

                                    qDebug() << "starting output file named " << outfilename << " status = " << status;

                                    for(int i=0;i<headerlines.size();i++)
                                    {
                                        *ostr << headerlines.at(i) << "\n";
                                    }

                                    *ostr << "rawfile=" + filepath << "\n";
                                    *ostr << "Pressure" << "\n";
                                }
                            }


                            if(inpumpcycle)
                            {
                                if(outputfile != nullptr)
                                {
                                    *ostr << queuedfilelines.dequeue() << "\n";
                                    pumpcylelen++;
                                }

                                if(pumpcylelen > 100000)
                                {
                                    inpumpcycle = false;
                                    //                                resetpumpcycle = true;

                                    if(outputfile != nullptr)
                                    {
                                        ostr->flush();
                                        outputfile->close();
                                        delete ostr;
                                        delete outputfile;
                                        ostr = nullptr;
                                        outputfile = nullptr;
                                    }

                                }

                            }
                        }

                    }



                    if(inputline.contains("-----------------------------"))
                    {
                        pastheader = true;
                        headerspopulated = true;
                        qDebug() << "past header = true";
                    }
                }

            }


        }

    }

}

void MainWindow::on_testsyncdetect_clicked()
{
    QString outpath = QFileDialog::getSaveFileName(this, "Specify output file", m_outputDir, "*.csv");

    if(!outpath.isEmpty())
    {
        testSyncCorrelation(outpath);
    }

}


void MainWindow::on_rxdelaytime_editingFinished()
{
    m_rxDelayTime = ui->rxdelaytime->text();
}

void MainWindow::on_txdelaytime_editingFinished()
{
    m_txDelayTime = ui->txdelaytime->text();
}

void MainWindow::on_synccorrelatornegscaling_editingFinished()
{
    m_negSyncScaling = ui->synccorrelatornegscaling->text();
}

void MainWindow::on_minsyncpulseheight_editingFinished()
{
    m_minSyncPulseHeight = ui->minsyncpulseheight->text();
}


void MainWindow::testSimpleFilters(QStringList inputfiles, QString bstop, QString outfile)
{
    FIRFilter bandstopfilter;
    MedianBiasedFilter mfilter(200, 0);

    mfilter.setNegClipping(true);
    bandstopfilter.loadFromFile(bstop);

    qDebug() << "bandstop filter has this many coeffecients " << bandstopfilter.getCoeffecientCount();

    ui->testfilter->setEnabled(false);
    m_cachedFiles = inputfiles;
    m_cachedPoints.clear();
    m_pointsPerFile = 0;
    ui->progressBar->setValue(0);

    QFile outputfile(outfile);
    QTextStream ostrm(&outputfile);
    int index = 0;
    int decimator = 0;

    outputfile.open(QFile::WriteOnly);

    ostrm << "index,original,bandstop,biased\n";

    while(haveMorePoints())
    {
        float value = nextPoint();
        QStringList values;

        if( ((decimator%10) == 0))
        {
            float bsvalue = bandstopfilter.nextValue(value);
            float pulsevalue = mfilter.nextValue(bsvalue);

            values << QString::number(index++) << QString::number(value) << QString::number(bsvalue) << QString::number(pulsevalue);

            QString msg = values.join(",");

            ostrm << msg << "\n";
        }

        decimator++;
    }

    qDebug() << "filter done";

    ui->testfilter->setEnabled(true);
    ui->progressBar->setValue(100);

}


void MainWindow::testSyncCorrelation(QString outputfilename)
{
    QHash<QString,QString> props = getMPDecoderProps();
    SyncDetector sdetector;
    FilterBlockImpl filterblock;
    TestDataSource datasrc;
    unsigned int count = 0;


    if(!props.contains(MEDIAN_FILTER_WINDOW_DEPTH))
    {
        float pulsecoverage = 2;

        if(props.contains(MEDIAN_FILTER_PULSE_SCALING))
        {
            QString coverage = props.value(MEDIAN_FILTER_PULSE_SCALING);

            pulsecoverage = coverage.toFloat();
        }

        float samplerate = props.value(RAW_SAMPLE_RATE).toFloat();
        float pulsewidth = props.value(PULSE_WIDTH).toFloat();
        int samplesinpulse = pulsewidth * samplerate;
        int depth = samplesinpulse * pulsecoverage;
        QString depthstr = QString::number(depth);

        props.insert(MEDIAN_FILTER_WINDOW_DEPTH, depthstr);
    }


    ui->decodestop->setEnabled(true);
    m_cachedFiles = m_sourcefiles;
    m_cachedPoints.clear();
    m_pointsPerFile = 0;
    ui->progressBar->setValue(0);
    m_abortDecode = false;

    qDebug() << "Starting syncdetector... processing this many files " << m_cachedFiles.size();
    dumpProperties(props);

    connect(&datasrc, SIGNAL(dataAvaliable(QList<float>)), &filterblock, SLOT(inputDataAvaliable(QList<float>)));
    connect(&filterblock, SIGNAL(dataAvaliable(QList<float>, QList<float>, float)), &sdetector, SLOT(dataAvaliable(QList<float>, QList<float>, float)));

    datasrc.initialize(props);
    filterblock.initialize(props);
    sdetector.initialize(props);

    while(true && haveMorePoints())
    {
       count++;
       datasrc.addData(nextPoint());

       if(count%50 == 0)
       {
           QApplication::processEvents();
       }

       if(m_pointsPerFile > 0)
       {
           int totalpoints = m_pointsPerFile * m_sourcefiles.size();
           int percent = ((float)count/(float)totalpoints) * 100;

           ui->progressBar->setValue(percent);
       }

       if(m_abortDecode)
       {
           break;
       }
    }

    ui->progressBar->setValue(100);
    ui->decodestop->setEnabled(false);

    QFile outfile(outputfilename);
    bool status = outfile.open(QFile::WriteOnly);
    QTextStream strm(&outfile);
    QList<QHash<QString,QString>> correlationdata = sdetector.getCorrelationData();
    QStringList headers = sdetector.getHeaders();

    strm << headers.join(",") << "\n";

    for(int i=0;i<correlationdata.size();i++)
    {
        QHash<QString,QString> rowdata = correlationdata.at(i);
        QStringList row;

        for(int j=0;j<headers.size();j++)
        {
            row << rowdata.value(headers.at(j));
        }

        strm << row.join(",") << "\n";
    }

}









TestDataSource::TestDataSource(QObject *parent):SerialDataSource (parent)
{

}

TestDataSource::~TestDataSource()
{

}

bool TestDataSource::initialize(QHash<QString,QString> properties)
{
    m_chunkSizeA = 100;
    m_useDownSampling = false;

    qDebug() << "!!dumping properties in TestDataSource...";
    MainWindow::dumpProperties(properties);

    if(properties.contains(RAWDATA_CHUNK_SIZE))
    {
        m_chunkSizeA = properties.value(RAWDATA_CHUNK_SIZE).toInt();
    }

    if(properties.contains(DOWNSAMPLEFROM) && properties.contains(DOWNSAMPLETO))
    {
        QString orgsamplerate = properties.value(DOWNSAMPLEFROM);
        QString samplerate = properties.value(DOWNSAMPLETO);

        qDebug() << "originalsample rate = " << orgsamplerate << " samplerate = " << samplerate;
        m_useDownSampling = (orgsamplerate != samplerate);
        m_downSampleRate = samplerate.toFloat();
        m_originalSampleRate = orgsamplerate.toFloat();
        qDebug() << "usedownsampling = " << m_useDownSampling;
    }
    else
    {
        m_useDownSampling = false;
    }

    qDebug() << "TestDataSource::initialize() use downsampling = " << m_useDownSampling << " downsamplerate =  " << m_downSampleRate << " original rate =  " << m_originalSampleRate << " chunck size = " << m_chunkSizeA;
    return SerialDataSource::initialize(properties);

}

float total = 0;
float count = 0;

void TestDataSource::addData(float value)
{
    m_cachedDataA << value;

    if(m_cachedDataA.size() >= m_chunkSizeA)
    {

        if(m_useDownSampling)
        {
            QList<float> ds_raw;

            resample(&m_cachedDataA, &ds_raw, m_originalSampleRate, m_downSampleRate);
//            resample(&m_cachedDataA, &ds_raw, 1000, 106.666666);
//            resample(&m_cachedDataA, &ds_raw, 1000, 100);

            long long start = QDateTime::currentMSecsSinceEpoch();
            emit dataAvaliable(ds_raw);
            long long delta = QDateTime::currentMSecsSinceEpoch() - start;

            total += delta;
            count++;
//            qCritical() << "TestDataSource #points = " << m_cachedDataA.size() << " delta = " << delta << " milliseconds " << " average = " << (total/count);
        }
        else
        {
            emit dataAvaliable(m_cachedDataA);
        }

        m_cachedDataA.clear();
    }
}

void TestDataSource::downSample(QList<float>* inputlist, QList<float>* outputlist, double rate1, double rate2)
{
    resample(inputlist, outputlist, rate1, rate2);
}



DataSampler::DataSampler(float samplerate, float downsampleRate)
{
    m_originalRate = samplerate;
    m_downsampleRate = downsampleRate;
}

DataSampler::~DataSampler()
{

}

void DataSampler::sampleData(QList<float>* original, QList<float>* downsampled)
{
    m_downsampler.downSample(original, downsampled, m_originalRate, m_downsampleRate);
}

QList<float> DataSampler::resampleData(QList<float>* original)
{
    QList<float> resampled;

    m_downsampler.downSample(original, &resampled, m_originalRate, m_downsampleRate);
    return resampled;
}





SyncDetector::SyncDetector()
{
    m_pfilter = nullptr;
    m_dataIndex = 0;
    m_headers << "time" << "raw" << "filtered" << "correlation" << "normalized" << "stdev" << "ismatched" << "matchcorrelation";
}

SyncDetector::~SyncDetector()
{
    if(m_pfilter != nullptr)
    {
        delete m_pfilter;
    }
}

bool SyncDetector::initialize(QHash<QString,QString> properties)
{
    int tuwidthinsamples = 0;
    int synccorrelationpercent = 0;
    float minsyncpulseamplitude = 2.0;
    int synccorrelationdelay = 10;

    if(properties.contains(SYNC_MIN_PULSE_THRESHOLD))
    {
        float threshold = properties.value(SYNC_MIN_PULSE_THRESHOLD).toFloat();

        minsyncpulseamplitude = threshold;
    }

    if(properties.contains(RAW_SAMPLE_RATE) && properties.contains(PULSE_WIDTH) && properties.contains(SYNC_CORRELATION_THRESHOLD))
    {

        float samplerate = properties.value(RAW_SAMPLE_RATE).toFloat();
        float pulsewidth = properties.value(PULSE_WIDTH).toFloat();
        int samplesinpulse = pulsewidth * samplerate;
        float negativescaling = 1.0F;

        if(properties.contains(SYNC_CORRELATION_NEG_SCALING))
        {
            negativescaling = properties.value(SYNC_CORRELATION_NEG_SCALING).toFloat();
        }


        if((int)samplerate == 106)
        {
            samplesinpulse = 40;
        }

        tuwidthinsamples = (pulsewidth/2) * samplerate;
        synccorrelationpercent = properties.value(SYNC_CORRELATION_THRESHOLD).toInt();

        if((int)samplerate == 106)
        {
            tuwidthinsamples = 20;

        }

        m_pulseWidthInSamples = samplesinpulse;

        QList<float> synccorrelationvalues = Utils::fillTemplate(Utils::getSyncTemplate(), true, 1.0F, tuwidthinsamples, negativescaling);
        m_pfilter = new SyncCorrelationFilter(synccorrelationpercent, tuwidthinsamples * 2, minsyncpulseamplitude, synccorrelationdelay * 2);
        m_pfilter->loadCoeffecients(synccorrelationvalues);
    }



    return false;
}

void SyncDetector::dataAvaliable(QList<float> raw, QList<float> filtered, float snr)
{
    for(int i=0;i<filtered.size();i++)
    {
        float rawval = raw.at(i);
        float filteredval = filtered.at(i);
        float correlation = m_pfilter->nextValue(filteredval);
//        float normalizedcorrelation = m_pfilter->normalizeCorrelation(correlation, m_pulseWidthInSamples);
        float matchcorrelaton = 0;
        bool ismatched = m_pfilter->isMatched(&matchcorrelaton);
        float normalizedcorrelation = m_pfilter->normalizeCorrelation(matchcorrelaton, m_pulseWidthInSamples);
        float stdev = m_pfilter->getStandardDev();
        int matchvalue = ismatched ? 5:0;
        QHash<QString,QString> rowmap;

        rowmap.insert("time", QString::number(m_dataIndex++));
        rowmap.insert("raw", QString::number(rawval));
        rowmap.insert("filtered", QString::number(filteredval));
        rowmap.insert("correlation", QString::number(correlation));
        rowmap.insert("stdev", QString::number(stdev));
        rowmap.insert("normalized", QString::number(normalizedcorrelation));
        rowmap.insert("ismatched", QString::number(matchvalue));
        rowmap.insert("matchcorrelation", QString::number(matchcorrelaton));
        m_correlationData << rowmap;

    }

}



SyncQualifier::SyncQualifier(int samplespertu, float minpulseheight, int detectiondelay)
{
    QList<int> peaklocations;

    m_minHeight = minpulseheight;
    m_detectDelay = detectiondelay;
    m_samplesPerTu = samplespertu;

    int totalsamples = (4 + (6 * 4) + 7) * samplespertu;

    peaklocations << 5 << 11 << 17 << 23;
    totalsamples += detectiondelay;

    for(int i=0;i<totalsamples;i++)
    {
        m_samples << 0.0F;
    }

    for(int i=0;i<peaklocations.size();i++)
    {
        m_peakLocations << peaklocations.at(i) * m_samplesPerTu;
    }

}

bool SyncQualifier::nextPoint(float val)
{
    bool qualified = false;

    m_samples << val;
    m_samples.removeLast();

    QHash<float, int> peakmap =  getPeaks();
    QList<float> peaks = peakmap.keys();

    qSort(peaks);

    if(peaks.size() >= 4)
    {
    }

    return qualified;
}

QHash<float, int> SyncQualifier::getPeaks()
{
    QHash<float, int> peaks;
    float currentpeak = 0;

    for(int i=0;i<m_samples.size() - m_detectDelay;i++)
    {
        float currval = m_samples.at(i);

        if(currval >= currentpeak)
        {
            currentpeak = currval;
        }
        else
        {
            if(currentpeak > m_minHeight)
            {
                peaks.insert(currentpeak, i);
            }

            currentpeak = 0;
        }
    }

    return peaks;
}

float SyncQualifier::qualifyPeaks(QList<int> peaklocations)
{
    float qualifier = 0;
    int totaloffset = 0;
    int maxoffset = 0;

    qSort(peaklocations);

    for(int i=0;i<m_peakLocations.size() && i<peaklocations.size();i++)
    {
        int peaklocation = m_peakLocations.at(i);
        int foundpeak = peaklocations.at(i);
        int offset = peaklocation - foundpeak;

        maxoffset = abs(offset) > maxoffset?abs(offset):maxoffset;
        totaloffset += (peaklocation - foundpeak);
    }

    qualifier = (float) abs(totaloffset)/(float) m_samplesPerTu;
    return qualifier;
}



