#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <qstring.h>
#include <qstringlist.h>
#include <qhash.h>
#include <qqueue.h>
#include "decoder.h"
#include "filterblock.h"
#include "datasources/serialdatasource.h"
#include "filters/synccorrelationfilter.h"




namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void loadSettings();
    void saveSettings();

    QString toSequenceString(QHash<QString,QString> sequencedefs);
    QHash<QString,QString> fromSequenceString(QString sequencedefs);
    QString getSSQTooltips(QHash<QString,QString> defs);
    QString getTFSQTooltips(QHash<QString,QString> defs);

    QHash<QString,QString> loadFileMeta(QString filename);
    QHash<QString,QString> getMPDecoderProps();
    QHash<QString,QString> getMPMultipleDecoderProps();
    QHash<QString,QString> getQPSKDecoderProps();

    void runDecoder(QHash<QString,QString> props);
    void writeProcessedData(QString filename);


    QList<float> loadPoints(QStringList filenames);
    QList<float> loadFile(QString filename);
    bool haveMorePoints();
    float nextPoint();
    void cachePoints(QStringList paths);

    void static dumpProperties(QHash<QString,QString> props);

    FilterBlock* getFilterBlock(QHash<QString,QString> props);


    // merge/split functionality...
    void mergeRawFiles(QStringList sourcefiles, QString target);
    QList<QHash<QString,QString>> readSurveyFile(QString path);
    QList<QHash<QString,QList<QList<QHash<QString, QString>>>>>  parseSurveys(QString filepath);
    void splitSurveyFile(QString inputfile, QString outputdir);
    void writeSurveyFile(QList<QHash<QString,QString>> surveydata, QString path, QStringList headers);

    void testSimpleFilters(QStringList inputfiles,QString bstop, QString outfile);

    void testSyncCorrelation(QString outputfilename);

private slots:
    void on_selectsourcefiles_clicked();

    void on_selectmpfilter_clicked();

    void on_runmp_clicked();

    void on_selectqpskfilter_clicked();

    void on_runqpsk_clicked();

    void on_selectoutputdir_clicked();

    void on_samplerate_editingFinished();

    void on_pulsewidth_editingFinished();

    void on_basefrequency_editingFinished();

    void on_cyclespersymbol_editingFinished();

    void on_trackdrift_stateChanged(int arg1);

    void on_usemudpulsediscriminator_stateChanged(int arg1);

    void on_downsamplerate_editingFinished();

    void on_decodestop_clicked();

    void filteredDataAvailable(QList<QPointF> raw, QList<QPointF> filtered, QList<QPointF> pressure, QHash<float,DecoderAnnotation*> annotations);

    void on_splitdecodes_clicked();

    void on_mergeraw_clicked();

    void on_stoponlostsymbol_stateChanged(int arg1);

    void on_lostsymbolcount_editingFinished();

    void on_mpusemultipleinstances_stateChanged(int arg1);

    void on_qpskusemultipleinstances_stateChanged(int arg1);

    void on_testfilter_clicked();

    void on_filtertestsamplerate_editingFinished();

    void on_testfilterdownsmplerate_editingFinished();

    void on_em_mcfgdirselect_clicked();

    void on_mp_mcfgdirselect_clicked();

    void on_pumpcycles_clicked();

    void on_testsyncdetect_clicked();

    void on_rxdelaytime_editingFinished();

    void on_txdelaytime_editingFinished();

    void on_synccorrelatornegscaling_editingFinished();

    void on_minsyncpulseheight_editingFinished();

private:
    Ui::MainWindow *ui;
    QStringList m_sourcefiles;
    QString m_surveySequence;
    QString m_toolfaceSequence;
    QString m_mpFilter;
    QString m_qpskFilter;
    bool m_useMPDiscriminator;
    QString m_pulseWidth;
    QString m_sampleFrequency;
    QString m_downsampleFrequency;
    QString m_outputDir;
    QString m_sourceDir;
    bool m_useDriftCorrection;
    QString m_baseFrequency;
    QString m_cylcesPerSymbol;
    bool m_stopOnLostSymbols;
    QString m_lostSymbolCount;
    bool m_qpskUseMutipleInstances;
    bool m_mpUseMultipleINstances;
    int m_currentTab;
    QString m_testFilterSampleRate;
    QString m_testFilterDownSampleRate;
    QString m_testFilterPath;
    QString m_testFilterOutPath;
    QString m_mpMultipleConfigurationPath;
    QString m_emMultipleConfigurationPath;


    QStringList m_cachedFiles;
    QQueue<float> m_cachedPoints;
    QList<QHash<QString, QString>> m_decodedData;
    QHash<float,DecoderAnnotation*> m_cachedAnnotations;
    int m_pointsPerFile;
    bool m_abortDecode;
    QHash<QString,QString> m_sequenceDefinitions;
    QString m_rxDelayTime;
    QString m_txDelayTime;
    QString m_negSyncScaling;
    QString m_minSyncPulseHeight;

};


class TestDataSource : public SerialDataSource
{
public:
    explicit TestDataSource(QObject *parent = 0);
    virtual ~TestDataSource();

    virtual bool initialize(QHash<QString,QString> properties);
    virtual void addData(float value);
    void downSample(QList<float>* inputlist, QList<float>* outputlist, double rate1, double rate2);

private:
    int m_chunkSizeA;
    QList<float> m_cachedDataA;
    double m_originalSampleRate;
};


class DataSampler
{
   public:
    DataSampler(float samplerate, float downsampleRate);
    virtual ~DataSampler();

    void sampleData(QList<float>* original, QList<float>* downsampled);
    QList<float> resampleData(QList<float>* original);

private:
    float m_originalRate;
    float m_downsampleRate;
    TestDataSource m_downsampler;

};



class SyncDetector : public QObject
{
    Q_OBJECT

public:
    SyncDetector();
    ~SyncDetector();

    bool initialize(QHash<QString,QString> properties);
    QList<QHash<QString,QString>> getCorrelationData(){return m_correlationData;}
    QStringList getHeaders(){return m_headers;}

private slots:
    void dataAvaliable(QList<float> raw, QList<float> filtered, float snr);

private:

    QList<QHash<QString,QString>> m_correlationData;
    QStringList m_headers;
    SyncCorrelationFilter* m_pfilter;
    int m_dataIndex;
    int m_pulseWidthInSamples;
};


class SyncQualifier
{
public:
    SyncQualifier(int samplespertu, float minpulseheight, int detectiondelay);

    bool nextPoint(float val);
    QHash<float, int> getPeaks();
    float qualifyPeaks(QList<int> peaklocations);

    int m_samplesPerTu;
    QList<float> m_samples;
    float m_minHeight;
    int m_detectDelay;
    QList<float> m_peakLocations;
};

#endif // MAINWINDOW_H
